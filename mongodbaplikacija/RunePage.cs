﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace mongodbaplikacija
{
  
   

    class RunePage
    {
        public ObjectId Id { get; set; }
        public string RuneName { get; set; }
        public string PrimaryRunes { get; set; }
        public string Keystone { get; set; }
        public string PrimarySlot1 { get; set; }
        public string PrimarySlot2 { get; set; }
        public string PrimarySlot3 { get; set; }
        public string SecondaryRunes { get; set; }
        public string SecondarySlot1 { get; set; }
        public string SecondarySlot2 { get; set; }
        public List<string> champions { get; set; }

        public RunePage()
        {
            champions = new List<string>();
        }
    }
}
