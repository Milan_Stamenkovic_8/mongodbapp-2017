﻿namespace mongodbaplikacija
{
    partial class AbilityForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AbilityForma));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAbilityName = new System.Windows.Forms.TextBox();
            this.checkBoxPassiveAbility = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAbilityCost = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxAbilityDescription = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.numUDAbilityRank = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAbilityShortcut = new System.Windows.Forms.TextBox();
            this.buttonCreateAbility = new System.Windows.Forms.Button();
            this.dgViewAbilities = new System.Windows.Forms.DataGridView();
            this.abilityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passiveAbility = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityShortcut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityRank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonViewAbilities = new System.Windows.Forms.Button();
            this.deleteAbility = new System.Windows.Forms.Button();
            this.btnSelectUpdate = new System.Windows.Forms.Button();
            this.btnUpdateAbility = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numUDAbilityRank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewAbilities)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // textBoxAbilityName
            // 
            this.textBoxAbilityName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.textBoxAbilityName.Location = new System.Drawing.Point(182, 57);
            this.textBoxAbilityName.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.textBoxAbilityName.Name = "textBoxAbilityName";
            this.textBoxAbilityName.Size = new System.Drawing.Size(180, 25);
            this.textBoxAbilityName.TabIndex = 1;
            // 
            // checkBoxPassiveAbility
            // 
            this.checkBoxPassiveAbility.AutoSize = true;
            this.checkBoxPassiveAbility.Location = new System.Drawing.Point(182, 99);
            this.checkBoxPassiveAbility.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.checkBoxPassiveAbility.Name = "checkBoxPassiveAbility";
            this.checkBoxPassiveAbility.Size = new System.Drawing.Size(95, 23);
            this.checkBoxPassiveAbility.TabIndex = 3;
            this.checkBoxPassiveAbility.Text = "Passive";
            this.checkBoxPassiveAbility.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 146);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cost:";
            // 
            // textBoxAbilityCost
            // 
            this.textBoxAbilityCost.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.textBoxAbilityCost.Location = new System.Drawing.Point(182, 146);
            this.textBoxAbilityCost.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.textBoxAbilityCost.Name = "textBoxAbilityCost";
            this.textBoxAbilityCost.Size = new System.Drawing.Size(180, 25);
            this.textBoxAbilityCost.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 212);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 19);
            this.label3.TabIndex = 6;
            this.label3.Text = "Description:";
            // 
            // textBoxAbilityDescription
            // 
            this.textBoxAbilityDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.textBoxAbilityDescription.Location = new System.Drawing.Point(182, 212);
            this.textBoxAbilityDescription.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.textBoxAbilityDescription.Name = "textBoxAbilityDescription";
            this.textBoxAbilityDescription.Size = new System.Drawing.Size(180, 25);
            this.textBoxAbilityDescription.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 273);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 19);
            this.label4.TabIndex = 8;
            this.label4.Text = "Rank:";
            // 
            // numUDAbilityRank
            // 
            this.numUDAbilityRank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.numUDAbilityRank.Location = new System.Drawing.Point(182, 273);
            this.numUDAbilityRank.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.numUDAbilityRank.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numUDAbilityRank.Name = "numUDAbilityRank";
            this.numUDAbilityRank.Size = new System.Drawing.Size(220, 25);
            this.numUDAbilityRank.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 322);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 19);
            this.label5.TabIndex = 10;
            this.label5.Text = "Shortcut:";
            // 
            // textBoxAbilityShortcut
            // 
            this.textBoxAbilityShortcut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.textBoxAbilityShortcut.Location = new System.Drawing.Point(182, 322);
            this.textBoxAbilityShortcut.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.textBoxAbilityShortcut.Name = "textBoxAbilityShortcut";
            this.textBoxAbilityShortcut.Size = new System.Drawing.Size(180, 25);
            this.textBoxAbilityShortcut.TabIndex = 11;
            // 
            // buttonCreateAbility
            // 
            this.buttonCreateAbility.BackColor = System.Drawing.Color.Aqua;
            this.buttonCreateAbility.Location = new System.Drawing.Point(182, 426);
            this.buttonCreateAbility.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.buttonCreateAbility.Name = "buttonCreateAbility";
            this.buttonCreateAbility.Size = new System.Drawing.Size(220, 34);
            this.buttonCreateAbility.TabIndex = 12;
            this.buttonCreateAbility.Text = "Create Ability";
            this.buttonCreateAbility.UseVisualStyleBackColor = false;
            this.buttonCreateAbility.Click += new System.EventHandler(this.buttonCreateAbility_Click);
            // 
            // dgViewAbilities
            // 
            this.dgViewAbilities.AllowUserToAddRows = false;
            this.dgViewAbilities.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgViewAbilities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgViewAbilities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.abilityName,
            this.passiveAbility,
            this.abilityShortcut,
            this.abilityRank,
            this.abilityCost,
            this.abilityDescription});
            this.dgViewAbilities.Location = new System.Drawing.Point(450, 57);
            this.dgViewAbilities.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.dgViewAbilities.Name = "dgViewAbilities";
            this.dgViewAbilities.Size = new System.Drawing.Size(687, 219);
            this.dgViewAbilities.TabIndex = 13;
            // 
            // abilityName
            // 
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Lime;
            this.abilityName.DefaultCellStyle = dataGridViewCellStyle1;
            this.abilityName.HeaderText = "Name";
            this.abilityName.Name = "abilityName";
            // 
            // passiveAbility
            // 
            this.passiveAbility.HeaderText = "Passive ability";
            this.passiveAbility.Name = "passiveAbility";
            // 
            // abilityShortcut
            // 
            this.abilityShortcut.HeaderText = "Shortcut";
            this.abilityShortcut.Name = "abilityShortcut";
            this.abilityShortcut.Width = 104;
            // 
            // abilityRank
            // 
            this.abilityRank.HeaderText = "Rank";
            this.abilityRank.Name = "abilityRank";
            // 
            // abilityCost
            // 
            this.abilityCost.HeaderText = "Cost";
            this.abilityCost.Name = "abilityCost";
            // 
            // abilityDescription
            // 
            this.abilityDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.abilityDescription.HeaderText = "Description";
            this.abilityDescription.Name = "abilityDescription";
            // 
            // buttonViewAbilities
            // 
            this.buttonViewAbilities.BackColor = System.Drawing.Color.Aqua;
            this.buttonViewAbilities.Location = new System.Drawing.Point(699, 426);
            this.buttonViewAbilities.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.buttonViewAbilities.Name = "buttonViewAbilities";
            this.buttonViewAbilities.Size = new System.Drawing.Size(138, 34);
            this.buttonViewAbilities.TabIndex = 14;
            this.buttonViewAbilities.Text = "View Abilities";
            this.buttonViewAbilities.UseVisualStyleBackColor = false;
            this.buttonViewAbilities.Click += new System.EventHandler(this.buttonViewAbilities_Click);
            // 
            // deleteAbility
            // 
            this.deleteAbility.BackColor = System.Drawing.Color.Aqua;
            this.deleteAbility.Location = new System.Drawing.Point(450, 322);
            this.deleteAbility.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.deleteAbility.Name = "deleteAbility";
            this.deleteAbility.Size = new System.Drawing.Size(138, 34);
            this.deleteAbility.TabIndex = 15;
            this.deleteAbility.Text = "Delete Ability";
            this.deleteAbility.UseVisualStyleBackColor = false;
            this.deleteAbility.Click += new System.EventHandler(this.deleteAbility_Click);
            // 
            // btnSelectUpdate
            // 
            this.btnSelectUpdate.BackColor = System.Drawing.Color.Aqua;
            this.btnSelectUpdate.Location = new System.Drawing.Point(699, 322);
            this.btnSelectUpdate.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnSelectUpdate.Name = "btnSelectUpdate";
            this.btnSelectUpdate.Size = new System.Drawing.Size(193, 34);
            this.btnSelectUpdate.TabIndex = 16;
            this.btnSelectUpdate.Text = "Select For Update";
            this.btnSelectUpdate.UseVisualStyleBackColor = false;
            this.btnSelectUpdate.Click += new System.EventHandler(this.btnSelectUpdate_Click);
            // 
            // btnUpdateAbility
            // 
            this.btnUpdateAbility.BackColor = System.Drawing.Color.Aqua;
            this.btnUpdateAbility.Location = new System.Drawing.Point(979, 322);
            this.btnUpdateAbility.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.btnUpdateAbility.Name = "btnUpdateAbility";
            this.btnUpdateAbility.Size = new System.Drawing.Size(158, 34);
            this.btnUpdateAbility.TabIndex = 17;
            this.btnUpdateAbility.Text = "Update Ability";
            this.btnUpdateAbility.UseVisualStyleBackColor = false;
            this.btnUpdateAbility.Click += new System.EventHandler(this.btnUpdateAbility_Click);
            // 
            // AbilityForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1179, 518);
            this.Controls.Add(this.btnUpdateAbility);
            this.Controls.Add(this.btnSelectUpdate);
            this.Controls.Add(this.deleteAbility);
            this.Controls.Add(this.buttonViewAbilities);
            this.Controls.Add(this.dgViewAbilities);
            this.Controls.Add(this.buttonCreateAbility);
            this.Controls.Add(this.textBoxAbilityShortcut);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numUDAbilityRank);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxAbilityDescription);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxAbilityCost);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBoxPassiveAbility);
            this.Controls.Add(this.textBoxAbilityName);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Blue;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.Name = "AbilityForma";
            this.Text = "AbilityForma";
            ((System.ComponentModel.ISupportInitialize)(this.numUDAbilityRank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewAbilities)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAbilityName;
        private System.Windows.Forms.CheckBox checkBoxPassiveAbility;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAbilityCost;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxAbilityDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numUDAbilityRank;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxAbilityShortcut;
        private System.Windows.Forms.Button buttonCreateAbility;
        private System.Windows.Forms.DataGridView dgViewAbilities;
        private System.Windows.Forms.Button buttonViewAbilities;
        private System.Windows.Forms.Button deleteAbility;
        private System.Windows.Forms.Button btnSelectUpdate;
        private System.Windows.Forms.Button btnUpdateAbility;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn passiveAbility;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityShortcut;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityRank;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityDescription;
    }
}