﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace mongodbaplikacija
{
    public partial class UpdateItem : Form
    {
        public UpdateItem()
        {
            InitializeComponent();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collection = db.GetCollection<Item>("ItemKolekcija");


            var filter = Builders<Item>.Filter.Eq("itemName", tbxItemName.Text);
            var result = collection.Find(filter).ToList();
            var updateDef = Builders<Item>.Update.Set(i => i.itemName, tbxItemName.Text).Set(i => i.itemDescription, tbxitemDescription.Text).Set(i => i.itemCost, nudItemCost.Value);

            collection.UpdateOne(filter, updateDef);

            if (result.Count != 0)
            {
                foreach (Item items in result)
                {
                    MessageBox.Show( "Updated item: " + items.itemName);
                }
            }
            else
            {
                MessageBox.Show("You have to specify item name for update!");
            }
            this.Close();
           
        }
    }
}
