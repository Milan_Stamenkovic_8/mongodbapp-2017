﻿namespace mongodbaplikacija
{
    partial class AddChampionForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddChampionForma));
            this.addchamp = new System.Windows.Forms.Button();
            this.champName = new System.Windows.Forms.Label();
            this.tbxChampname = new System.Windows.Forms.TextBox();
            this.role = new System.Windows.Forms.Label();
            this.cbxRole = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxPrefLane = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxMagicResource = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbxhp = new System.Windows.Forms.TextBox();
            this.tbxas = new System.Windows.Forms.TextBox();
            this.tbxad = new System.Windows.Forms.TextBox();
            this.tbxa = new System.Windows.Forms.TextBox();
            this.tbxm = new System.Windows.Forms.TextBox();
            this.tbxap = new System.Windows.Forms.TextBox();
            this.tbxms = new System.Windows.Forms.TextBox();
            this.tbxmr = new System.Windows.Forms.TextBox();
            this.addItem = new System.Windows.Forms.Button();
            this.addRune = new System.Windows.Forms.Button();
            this.addAbility = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbxStrong = new System.Windows.Forms.TextBox();
            this.tbxWeak = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // addchamp
            // 
            this.addchamp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.addchamp.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addchamp.ForeColor = System.Drawing.SystemColors.Highlight;
            this.addchamp.Location = new System.Drawing.Point(48, 307);
            this.addchamp.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.addchamp.Name = "addchamp";
            this.addchamp.Size = new System.Drawing.Size(186, 43);
            this.addchamp.TabIndex = 0;
            this.addchamp.Text = "Create Champion";
            this.addchamp.UseVisualStyleBackColor = false;
            this.addchamp.Click += new System.EventHandler(this.addChamp_Click);
            // 
            // champName
            // 
            this.champName.AutoSize = true;
            this.champName.Location = new System.Drawing.Point(44, 66);
            this.champName.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.champName.Name = "champName";
            this.champName.Size = new System.Drawing.Size(149, 19);
            this.champName.TabIndex = 1;
            this.champName.Text = "Champion Name:";
            // 
            // tbxChampname
            // 
            this.tbxChampname.ForeColor = System.Drawing.Color.Red;
            this.tbxChampname.Location = new System.Drawing.Point(227, 60);
            this.tbxChampname.Name = "tbxChampname";
            this.tbxChampname.Size = new System.Drawing.Size(166, 25);
            this.tbxChampname.TabIndex = 2;
            // 
            // role
            // 
            this.role.AutoSize = true;
            this.role.Location = new System.Drawing.Point(44, 110);
            this.role.Name = "role";
            this.role.Size = new System.Drawing.Size(52, 19);
            this.role.TabIndex = 3;
            this.role.Text = "Role:";
            // 
            // cbxRole
            // 
            this.cbxRole.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxRole.ForeColor = System.Drawing.Color.Red;
            this.cbxRole.FormattingEnabled = true;
            this.cbxRole.Items.AddRange(new object[] {
            "Assassin",
            "Fighter",
            "Mage",
            "Marksman",
            "Support",
            "Tank"});
            this.cbxRole.Location = new System.Drawing.Point(227, 108);
            this.cbxRole.Name = "cbxRole";
            this.cbxRole.Size = new System.Drawing.Size(121, 24);
            this.cbxRole.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 160);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 19);
            this.label1.TabIndex = 5;
            this.label1.Text = "Preffered Lane:";
            // 
            // cbxPrefLane
            // 
            this.cbxPrefLane.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxPrefLane.ForeColor = System.Drawing.Color.Red;
            this.cbxPrefLane.FormattingEnabled = true;
            this.cbxPrefLane.Items.AddRange(new object[] {
            "Top",
            "Jungle",
            "Middle",
            "Bottom"});
            this.cbxPrefLane.Location = new System.Drawing.Point(227, 155);
            this.cbxPrefLane.Name = "cbxPrefLane";
            this.cbxPrefLane.Size = new System.Drawing.Size(121, 24);
            this.cbxPrefLane.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 205);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 19);
            this.label2.TabIndex = 7;
            this.label2.Text = "Magic Resource:";
            // 
            // cbxMagicResource
            // 
            this.cbxMagicResource.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxMagicResource.ForeColor = System.Drawing.Color.Red;
            this.cbxMagicResource.FormattingEnabled = true;
            this.cbxMagicResource.Items.AddRange(new object[] {
            "Mana",
            "Energy",
            "Rage",
            "Combo Points",
            "Ferocity Points"});
            this.cbxMagicResource.Location = new System.Drawing.Point(227, 200);
            this.cbxMagicResource.Name = "cbxMagicResource";
            this.cbxMagicResource.Size = new System.Drawing.Size(121, 24);
            this.cbxMagicResource.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(444, 22);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 19);
            this.label3.TabIndex = 9;
            this.label3.Text = "Base Stats:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(444, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 19);
            this.label4.TabIndex = 10;
            this.label4.Text = "Health:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(729, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 19);
            this.label5.TabIndex = 11;
            this.label5.Text = "Mana:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(444, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 19);
            this.label6.TabIndex = 12;
            this.label6.Text = "Attack Damage:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(444, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 19);
            this.label7.TabIndex = 13;
            this.label7.Text = "Attack Speed:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(444, 204);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 19);
            this.label8.TabIndex = 14;
            this.label8.Text = "Armor:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(729, 204);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(171, 19);
            this.label9.TabIndex = 15;
            this.label9.Text = "Magic Resistance:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(729, 160);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(159, 19);
            this.label10.TabIndex = 16;
            this.label10.Text = "Movement Speed:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(729, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(134, 19);
            this.label11.TabIndex = 17;
            this.label11.Text = "Ability Power:";
            // 
            // tbxhp
            // 
            this.tbxhp.ForeColor = System.Drawing.Color.Red;
            this.tbxhp.Location = new System.Drawing.Point(607, 60);
            this.tbxhp.Name = "tbxhp";
            this.tbxhp.Size = new System.Drawing.Size(88, 25);
            this.tbxhp.TabIndex = 18;
            // 
            // tbxas
            // 
            this.tbxas.ForeColor = System.Drawing.Color.Red;
            this.tbxas.Location = new System.Drawing.Point(607, 153);
            this.tbxas.Name = "tbxas";
            this.tbxas.Size = new System.Drawing.Size(88, 25);
            this.tbxas.TabIndex = 19;
            // 
            // tbxad
            // 
            this.tbxad.ForeColor = System.Drawing.Color.Red;
            this.tbxad.Location = new System.Drawing.Point(607, 104);
            this.tbxad.Name = "tbxad";
            this.tbxad.Size = new System.Drawing.Size(88, 25);
            this.tbxad.TabIndex = 20;
            // 
            // tbxa
            // 
            this.tbxa.ForeColor = System.Drawing.Color.Red;
            this.tbxa.Location = new System.Drawing.Point(607, 198);
            this.tbxa.Name = "tbxa";
            this.tbxa.Size = new System.Drawing.Size(88, 25);
            this.tbxa.TabIndex = 21;
            // 
            // tbxm
            // 
            this.tbxm.ForeColor = System.Drawing.Color.Red;
            this.tbxm.Location = new System.Drawing.Point(938, 63);
            this.tbxm.Name = "tbxm";
            this.tbxm.Size = new System.Drawing.Size(88, 25);
            this.tbxm.TabIndex = 22;
            // 
            // tbxap
            // 
            this.tbxap.ForeColor = System.Drawing.Color.Red;
            this.tbxap.Location = new System.Drawing.Point(938, 104);
            this.tbxap.Name = "tbxap";
            this.tbxap.Size = new System.Drawing.Size(88, 25);
            this.tbxap.TabIndex = 23;
            // 
            // tbxms
            // 
            this.tbxms.ForeColor = System.Drawing.Color.Red;
            this.tbxms.Location = new System.Drawing.Point(938, 153);
            this.tbxms.Name = "tbxms";
            this.tbxms.Size = new System.Drawing.Size(88, 25);
            this.tbxms.TabIndex = 24;
            // 
            // tbxmr
            // 
            this.tbxmr.ForeColor = System.Drawing.Color.Red;
            this.tbxmr.Location = new System.Drawing.Point(938, 201);
            this.tbxmr.Name = "tbxmr";
            this.tbxmr.Size = new System.Drawing.Size(88, 25);
            this.tbxmr.TabIndex = 25;
            // 
            // addItem
            // 
            this.addItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.addItem.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.addItem.Location = new System.Drawing.Point(305, 307);
            this.addItem.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.addItem.Name = "addItem";
            this.addItem.Size = new System.Drawing.Size(186, 43);
            this.addItem.TabIndex = 26;
            this.addItem.Text = "Add Item";
            this.addItem.UseVisualStyleBackColor = false;
            this.addItem.Click += new System.EventHandler(this.addItem_Click);
            // 
            // addRune
            // 
            this.addRune.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.addRune.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addRune.ForeColor = System.Drawing.SystemColors.Highlight;
            this.addRune.Location = new System.Drawing.Point(578, 307);
            this.addRune.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.addRune.Name = "addRune";
            this.addRune.Size = new System.Drawing.Size(186, 43);
            this.addRune.TabIndex = 27;
            this.addRune.Text = "Add Rune Page";
            this.addRune.UseVisualStyleBackColor = false;
            this.addRune.Click += new System.EventHandler(this.addRune_Click);
            // 
            // addAbility
            // 
            this.addAbility.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.addAbility.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addAbility.ForeColor = System.Drawing.SystemColors.Highlight;
            this.addAbility.Location = new System.Drawing.Point(840, 307);
            this.addAbility.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.addAbility.Name = "addAbility";
            this.addAbility.Size = new System.Drawing.Size(186, 43);
            this.addAbility.TabIndex = 28;
            this.addAbility.Text = "Add Ability";
            this.addAbility.UseVisualStyleBackColor = false;
            this.addAbility.Click += new System.EventHandler(this.addAbility_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(244, 258);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(149, 19);
            this.label12.TabIndex = 29;
            this.label12.Text = "Strong Against:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(656, 261);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(137, 19);
            this.label13.TabIndex = 32;
            this.label13.Text = "Weak Against:";
            // 
            // tbxStrong
            // 
            this.tbxStrong.ForeColor = System.Drawing.Color.Red;
            this.tbxStrong.Location = new System.Drawing.Point(429, 255);
            this.tbxStrong.Name = "tbxStrong";
            this.tbxStrong.Size = new System.Drawing.Size(166, 25);
            this.tbxStrong.TabIndex = 33;
            // 
            // tbxWeak
            // 
            this.tbxWeak.ForeColor = System.Drawing.Color.Red;
            this.tbxWeak.Location = new System.Drawing.Point(860, 255);
            this.tbxWeak.Name = "tbxWeak";
            this.tbxWeak.Size = new System.Drawing.Size(166, 25);
            this.tbxWeak.TabIndex = 34;
            // 
            // AddChampionForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(1051, 363);
            this.Controls.Add(this.tbxWeak);
            this.Controls.Add(this.tbxStrong);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.addAbility);
            this.Controls.Add(this.addRune);
            this.Controls.Add(this.addItem);
            this.Controls.Add(this.tbxmr);
            this.Controls.Add(this.tbxms);
            this.Controls.Add(this.tbxap);
            this.Controls.Add(this.tbxm);
            this.Controls.Add(this.tbxa);
            this.Controls.Add(this.tbxad);
            this.Controls.Add(this.tbxas);
            this.Controls.Add(this.tbxhp);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbxMagicResource);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbxPrefLane);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxRole);
            this.Controls.Add(this.role);
            this.Controls.Add(this.tbxChampname);
            this.Controls.Add(this.champName);
            this.Controls.Add(this.addchamp);
            this.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.Name = "AddChampionForma";
            this.Text = "Create Champion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addchamp;
        private System.Windows.Forms.Label champName;
        private System.Windows.Forms.TextBox tbxChampname;
        private System.Windows.Forms.Label role;
        private System.Windows.Forms.ComboBox cbxRole;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxPrefLane;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxMagicResource;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbxhp;
        private System.Windows.Forms.TextBox tbxas;
        private System.Windows.Forms.TextBox tbxad;
        private System.Windows.Forms.TextBox tbxa;
        private System.Windows.Forms.TextBox tbxm;
        private System.Windows.Forms.TextBox tbxap;
        private System.Windows.Forms.TextBox tbxms;
        private System.Windows.Forms.TextBox tbxmr;
        private System.Windows.Forms.Button addItem;
        private System.Windows.Forms.Button addRune;
        private System.Windows.Forms.Button addAbility;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbxStrong;
        private System.Windows.Forms.TextBox tbxWeak;
    }
}