﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace mongodbaplikacija
{   
   
    class Ability
    {
        public ObjectId Id { get; set; }
        public string AbilityName { get; set; }
        public bool PassiveAbility { get; set; }
        public string AbilityCost { get; set; }
        public string AbilityDescription { get; set; }
        public int AbilityRank { get; set; }
        public string AbilityShortcut { get; set; }
        public List<string> champions { get; set; }
        
        public Ability()
        {
            champions = new List<string>();
        }
    }
}
