﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace mongodbaplikacija
{
    class Item
    {
        public ObjectId Id { get; set; }
        public string itemName { get; set; }
        public List<string> itemType { get; set; }
        public int itemCost { get; set; }
        public string itemDescription { get; set; }
        public List<string> champions { get; set; }

        public Item()
        {
            itemType = new List<string>();
            champions = new List<string>();
        }
    }
}
