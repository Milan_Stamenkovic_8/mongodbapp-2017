﻿namespace mongodbaplikacija
{
    partial class RuneForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RuneForma));
            this.crt_rune = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxPrimary = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxSecondary = new System.Windows.Forms.ComboBox();
            this.comboKeystone = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboPrimarySlot1 = new System.Windows.Forms.ComboBox();
            this.comboPrimarySlot2 = new System.Windows.Forms.ComboBox();
            this.comboPrimarySlot3 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboSecondarySlot1 = new System.Windows.Forms.ComboBox();
            this.comboSecondarySlot2 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboSecondarySlot3 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonViewRunes = new System.Windows.Forms.Button();
            this.checkIzaberi1 = new System.Windows.Forms.CheckBox();
            this.checkIzaberi2 = new System.Windows.Forms.CheckBox();
            this.checkIzaberi3 = new System.Windows.Forms.CheckBox();
            this.dataGridRunes = new System.Windows.Forms.DataGridView();
            this.RuneName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimaryRune = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Keystone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimarySlot1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimarySlot2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimarySlot3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecondaryRune = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecondarySlot1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecondarySlot2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDeleteRune = new System.Windows.Forms.Button();
            this.btnSelectUpdateRune = new System.Windows.Forms.Button();
            this.btnUpdateRune = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxRuneName = new System.Windows.Forms.TextBox();
            this.runePageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRunes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.runePageBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // crt_rune
            // 
            this.crt_rune.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.crt_rune.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crt_rune.ForeColor = System.Drawing.Color.Blue;
            this.crt_rune.Location = new System.Drawing.Point(24, 536);
            this.crt_rune.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.crt_rune.Name = "crt_rune";
            this.crt_rune.Size = new System.Drawing.Size(253, 37);
            this.crt_rune.TabIndex = 0;
            this.crt_rune.Text = "Create Rune Page";
            this.crt_rune.UseVisualStyleBackColor = false;
            this.crt_rune.Click += new System.EventHandler(this.crt_rune_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(20, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Primary Runes:";
            // 
            // cbxPrimary
            // 
            this.cbxPrimary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.cbxPrimary.Font = new System.Drawing.Font("Stencil", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxPrimary.ForeColor = System.Drawing.SystemColors.Desktop;
            this.cbxPrimary.FormattingEnabled = true;
            this.cbxPrimary.Items.AddRange(new object[] {
            "Precision",
            "Domination",
            "Sorcery",
            "Resolve",
            "Inspiration"});
            this.cbxPrimary.Location = new System.Drawing.Point(260, 29);
            this.cbxPrimary.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cbxPrimary.Name = "cbxPrimary";
            this.cbxPrimary.Size = new System.Drawing.Size(262, 24);
            this.cbxPrimary.TabIndex = 2;
            this.cbxPrimary.SelectedIndexChanged += new System.EventHandler(this.cbxPrimary_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Aqua;
            this.label2.Location = new System.Drawing.Point(21, 185);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(193, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Select Secondary Runes:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // cbxSecondary
            // 
            this.cbxSecondary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.cbxSecondary.Font = new System.Drawing.Font("Stencil", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxSecondary.ForeColor = System.Drawing.SystemColors.Desktop;
            this.cbxSecondary.FormattingEnabled = true;
            this.cbxSecondary.Items.AddRange(new object[] {
            "Precision",
            "Domination",
            "Sorcery",
            "Resolve",
            "Inspiration"});
            this.cbxSecondary.Location = new System.Drawing.Point(260, 178);
            this.cbxSecondary.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cbxSecondary.Name = "cbxSecondary";
            this.cbxSecondary.Size = new System.Drawing.Size(262, 24);
            this.cbxSecondary.TabIndex = 4;
            this.cbxSecondary.SelectedIndexChanged += new System.EventHandler(this.cbxSecondary_SelectedIndexChanged);
            // 
            // comboKeystone
            // 
            this.comboKeystone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.comboKeystone.FormattingEnabled = true;
            this.comboKeystone.Location = new System.Drawing.Point(120, 77);
            this.comboKeystone.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.comboKeystone.Name = "comboKeystone";
            this.comboKeystone.Size = new System.Drawing.Size(199, 25);
            this.comboKeystone.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(20, 85);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Keystone:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(37, 131);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Slot 1:";
            // 
            // comboPrimarySlot1
            // 
            this.comboPrimarySlot1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.comboPrimarySlot1.FormattingEnabled = true;
            this.comboPrimarySlot1.Location = new System.Drawing.Point(120, 121);
            this.comboPrimarySlot1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.comboPrimarySlot1.Name = "comboPrimarySlot1";
            this.comboPrimarySlot1.Size = new System.Drawing.Size(199, 25);
            this.comboPrimarySlot1.TabIndex = 9;
            // 
            // comboPrimarySlot2
            // 
            this.comboPrimarySlot2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.comboPrimarySlot2.FormattingEnabled = true;
            this.comboPrimarySlot2.Location = new System.Drawing.Point(442, 123);
            this.comboPrimarySlot2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.comboPrimarySlot2.Name = "comboPrimarySlot2";
            this.comboPrimarySlot2.Size = new System.Drawing.Size(199, 25);
            this.comboPrimarySlot2.TabIndex = 10;
            // 
            // comboPrimarySlot3
            // 
            this.comboPrimarySlot3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.comboPrimarySlot3.FormattingEnabled = true;
            this.comboPrimarySlot3.Location = new System.Drawing.Point(821, 126);
            this.comboPrimarySlot3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.comboPrimarySlot3.Name = "comboPrimarySlot3";
            this.comboPrimarySlot3.Size = new System.Drawing.Size(199, 25);
            this.comboPrimarySlot3.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(353, 131);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Slot 2:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(718, 131);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Slot 3:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Aqua;
            this.label7.Location = new System.Drawing.Point(37, 245);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "Slot 1:";
            // 
            // comboSecondarySlot1
            // 
            this.comboSecondarySlot1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.comboSecondarySlot1.FormattingEnabled = true;
            this.comboSecondarySlot1.Location = new System.Drawing.Point(120, 237);
            this.comboSecondarySlot1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.comboSecondarySlot1.Name = "comboSecondarySlot1";
            this.comboSecondarySlot1.Size = new System.Drawing.Size(199, 25);
            this.comboSecondarySlot1.TabIndex = 15;
            // 
            // comboSecondarySlot2
            // 
            this.comboSecondarySlot2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.comboSecondarySlot2.FormattingEnabled = true;
            this.comboSecondarySlot2.Location = new System.Drawing.Point(442, 237);
            this.comboSecondarySlot2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.comboSecondarySlot2.Name = "comboSecondarySlot2";
            this.comboSecondarySlot2.Size = new System.Drawing.Size(199, 25);
            this.comboSecondarySlot2.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Aqua;
            this.label8.Location = new System.Drawing.Point(353, 245);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Slot 2:";
            // 
            // comboSecondarySlot3
            // 
            this.comboSecondarySlot3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.comboSecondarySlot3.FormattingEnabled = true;
            this.comboSecondarySlot3.Location = new System.Drawing.Point(821, 237);
            this.comboSecondarySlot3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.comboSecondarySlot3.Name = "comboSecondarySlot3";
            this.comboSecondarySlot3.Size = new System.Drawing.Size(199, 25);
            this.comboSecondarySlot3.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Aqua;
            this.label9.Location = new System.Drawing.Point(718, 245);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 17);
            this.label9.TabIndex = 19;
            this.label9.Text = "Slot 3:";
            // 
            // buttonViewRunes
            // 
            this.buttonViewRunes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.buttonViewRunes.ForeColor = System.Drawing.Color.Yellow;
            this.buttonViewRunes.Location = new System.Drawing.Point(310, 536);
            this.buttonViewRunes.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonViewRunes.Name = "buttonViewRunes";
            this.buttonViewRunes.Size = new System.Drawing.Size(187, 37);
            this.buttonViewRunes.TabIndex = 20;
            this.buttonViewRunes.Text = "View Rune Pages";
            this.buttonViewRunes.UseVisualStyleBackColor = false;
            this.buttonViewRunes.Click += new System.EventHandler(this.buttonViewRunes_Click);
            // 
            // checkIzaberi1
            // 
            this.checkIzaberi1.AutoSize = true;
            this.checkIzaberi1.ForeColor = System.Drawing.Color.Aqua;
            this.checkIzaberi1.Location = new System.Drawing.Point(120, 270);
            this.checkIzaberi1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.checkIzaberi1.Name = "checkIzaberi1";
            this.checkIzaberi1.Size = new System.Drawing.Size(78, 21);
            this.checkIzaberi1.TabIndex = 21;
            this.checkIzaberi1.Text = "Choose";
            this.checkIzaberi1.UseVisualStyleBackColor = true;
            this.checkIzaberi1.CheckedChanged += new System.EventHandler(this.checkIzaberi1_CheckedChanged);
            // 
            // checkIzaberi2
            // 
            this.checkIzaberi2.AutoSize = true;
            this.checkIzaberi2.ForeColor = System.Drawing.Color.Aqua;
            this.checkIzaberi2.Location = new System.Drawing.Point(442, 270);
            this.checkIzaberi2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.checkIzaberi2.Name = "checkIzaberi2";
            this.checkIzaberi2.Size = new System.Drawing.Size(78, 21);
            this.checkIzaberi2.TabIndex = 22;
            this.checkIzaberi2.Text = "Choose";
            this.checkIzaberi2.UseVisualStyleBackColor = true;
            this.checkIzaberi2.CheckedChanged += new System.EventHandler(this.checkIzaberi2_CheckedChanged);
            // 
            // checkIzaberi3
            // 
            this.checkIzaberi3.AutoSize = true;
            this.checkIzaberi3.ForeColor = System.Drawing.Color.Aqua;
            this.checkIzaberi3.Location = new System.Drawing.Point(821, 270);
            this.checkIzaberi3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.checkIzaberi3.Name = "checkIzaberi3";
            this.checkIzaberi3.Size = new System.Drawing.Size(78, 21);
            this.checkIzaberi3.TabIndex = 23;
            this.checkIzaberi3.Text = "Choose";
            this.checkIzaberi3.UseVisualStyleBackColor = true;
            this.checkIzaberi3.CheckedChanged += new System.EventHandler(this.checkIzaberi3_CheckedChanged);
            // 
            // dataGridRunes
            // 
            this.dataGridRunes.AllowUserToAddRows = false;
            this.dataGridRunes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dataGridRunes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRunes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RuneName,
            this.PrimaryRune,
            this.Keystone,
            this.PrimarySlot1,
            this.PrimarySlot2,
            this.PrimarySlot3,
            this.SecondaryRune,
            this.SecondarySlot1,
            this.SecondarySlot2});
            this.dataGridRunes.Location = new System.Drawing.Point(23, 309);
            this.dataGridRunes.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.dataGridRunes.Name = "dataGridRunes";
            this.dataGridRunes.Size = new System.Drawing.Size(997, 196);
            this.dataGridRunes.TabIndex = 24;
            this.dataGridRunes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridRunes_CellContentClick);
            // 
            // RuneName
            // 
            this.RuneName.HeaderText = "Rune Name";
            this.RuneName.Name = "RuneName";
            // 
            // PrimaryRune
            // 
            this.PrimaryRune.HeaderText = "Primary rune";
            this.PrimaryRune.Name = "PrimaryRune";
            // 
            // Keystone
            // 
            this.Keystone.HeaderText = "Keystone";
            this.Keystone.Name = "Keystone";
            // 
            // PrimarySlot1
            // 
            this.PrimarySlot1.HeaderText = "Primary Slot1";
            this.PrimarySlot1.Name = "PrimarySlot1";
            // 
            // PrimarySlot2
            // 
            this.PrimarySlot2.HeaderText = "Primary Slot 2";
            this.PrimarySlot2.Name = "PrimarySlot2";
            // 
            // PrimarySlot3
            // 
            this.PrimarySlot3.HeaderText = "Primary Slot 3";
            this.PrimarySlot3.Name = "PrimarySlot3";
            // 
            // SecondaryRune
            // 
            this.SecondaryRune.HeaderText = "Secondary rune";
            this.SecondaryRune.Name = "SecondaryRune";
            // 
            // SecondarySlot1
            // 
            this.SecondarySlot1.HeaderText = "Secondary Slot 1";
            this.SecondarySlot1.Name = "SecondarySlot1";
            // 
            // SecondarySlot2
            // 
            this.SecondarySlot2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SecondarySlot2.HeaderText = "Secondary Slot 2";
            this.SecondarySlot2.Name = "SecondarySlot2";
            // 
            // btnDeleteRune
            // 
            this.btnDeleteRune.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnDeleteRune.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteRune.Location = new System.Drawing.Point(528, 536);
            this.btnDeleteRune.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnDeleteRune.Name = "btnDeleteRune";
            this.btnDeleteRune.Size = new System.Drawing.Size(125, 37);
            this.btnDeleteRune.TabIndex = 25;
            this.btnDeleteRune.Text = "Delete Runes";
            this.btnDeleteRune.UseVisualStyleBackColor = false;
            this.btnDeleteRune.Click += new System.EventHandler(this.btnDeleteRune_Click);
            // 
            // btnSelectUpdateRune
            // 
            this.btnSelectUpdateRune.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnSelectUpdateRune.ForeColor = System.Drawing.Color.Aqua;
            this.btnSelectUpdateRune.Location = new System.Drawing.Point(684, 536);
            this.btnSelectUpdateRune.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnSelectUpdateRune.Name = "btnSelectUpdateRune";
            this.btnSelectUpdateRune.Size = new System.Drawing.Size(182, 37);
            this.btnSelectUpdateRune.TabIndex = 26;
            this.btnSelectUpdateRune.Text = "Select For Update";
            this.btnSelectUpdateRune.UseVisualStyleBackColor = false;
            this.btnSelectUpdateRune.Click += new System.EventHandler(this.btnSelectUpdateRune_Click);
            // 
            // btnUpdateRune
            // 
            this.btnUpdateRune.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnUpdateRune.ForeColor = System.Drawing.Color.Lime;
            this.btnUpdateRune.Location = new System.Drawing.Point(895, 536);
            this.btnUpdateRune.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnUpdateRune.Name = "btnUpdateRune";
            this.btnUpdateRune.Size = new System.Drawing.Size(125, 37);
            this.btnUpdateRune.TabIndex = 27;
            this.btnUpdateRune.Text = "Update Runes";
            this.btnUpdateRune.UseVisualStyleBackColor = false;
            this.btnUpdateRune.Click += new System.EventHandler(this.btnUpdateRune_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(592, 36);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 17);
            this.label10.TabIndex = 28;
            this.label10.Text = "Rune Name:";
            // 
            // textBoxRuneName
            // 
            this.textBoxRuneName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxRuneName.Location = new System.Drawing.Point(721, 29);
            this.textBoxRuneName.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBoxRuneName.Name = "textBoxRuneName";
            this.textBoxRuneName.Size = new System.Drawing.Size(164, 24);
            this.textBoxRuneName.TabIndex = 29;
            // 
            // runePageBindingSource
            // 
            this.runePageBindingSource.DataSource = typeof(mongodbaplikacija.RunePage);
            // 
            // RuneForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(1068, 622);
            this.Controls.Add(this.textBoxRuneName);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnUpdateRune);
            this.Controls.Add(this.btnSelectUpdateRune);
            this.Controls.Add(this.btnDeleteRune);
            this.Controls.Add(this.dataGridRunes);
            this.Controls.Add(this.checkIzaberi3);
            this.Controls.Add(this.checkIzaberi2);
            this.Controls.Add(this.checkIzaberi1);
            this.Controls.Add(this.buttonViewRunes);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboSecondarySlot3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboSecondarySlot2);
            this.Controls.Add(this.comboSecondarySlot1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboPrimarySlot3);
            this.Controls.Add(this.comboPrimarySlot2);
            this.Controls.Add(this.comboPrimarySlot1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboKeystone);
            this.Controls.Add(this.cbxSecondary);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbxPrimary);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.crt_rune);
            this.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "RuneForma";
            this.Text = "Rune Page";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRunes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.runePageBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button crt_rune;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxPrimary;
        private System.Windows.Forms.BindingSource runePageBindingSource;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxSecondary;
        private System.Windows.Forms.ComboBox comboKeystone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboPrimarySlot1;
        private System.Windows.Forms.ComboBox comboPrimarySlot2;
        private System.Windows.Forms.ComboBox comboPrimarySlot3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboSecondarySlot1;
        private System.Windows.Forms.ComboBox comboSecondarySlot2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboSecondarySlot3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonViewRunes;
        private System.Windows.Forms.CheckBox checkIzaberi1;
        private System.Windows.Forms.CheckBox checkIzaberi2;
        private System.Windows.Forms.CheckBox checkIzaberi3;
        private System.Windows.Forms.DataGridView dataGridRunes;
        private System.Windows.Forms.Button btnDeleteRune;
        private System.Windows.Forms.Button btnSelectUpdateRune;
        private System.Windows.Forms.Button btnUpdateRune;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxRuneName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RuneName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimaryRune;
        private System.Windows.Forms.DataGridViewTextBoxColumn Keystone;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimarySlot1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimarySlot2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimarySlot3;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecondaryRune;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecondarySlot1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecondarySlot2;
    }
}