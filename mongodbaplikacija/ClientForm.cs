﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace mongodbaplikacija
{
    public partial class Lolnexus : Form
    {
        public Lolnexus()
        {
            InitializeComponent();
        }

        private void addchamp_Click(object sender, EventArgs e)
        {

            var addChampionForma = new AddChampionForma();
            addChampionForma.Show();

        }

        private void quit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure You Want To Exit ?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void lolbuild_Click(object sender, EventArgs e)
        {
            var itemForma = new ItemForm();
            itemForma.Show();
        }

        private void lolrunes_Click(object sender, EventArgs e)
        {
            var runeForma = new RuneForma();
            runeForma.Show();
        }

        private void lolspells_Click(object sender, EventArgs e)
        {
            var abilityForma = new AbilityForma();
            abilityForma.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var allChampionsForma = new AllChampionsForma();
            allChampionsForma.Show();
        }
    }
}
