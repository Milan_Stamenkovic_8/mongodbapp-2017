﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;



namespace mongodbaplikacija
{
   
    public partial class AddChampionForma : Form
    {
        public AddChampionForma()
        {
            InitializeComponent();
        }

        private void addChamp_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            //db.DropCollection("championKolekcija");
            var Champ_collection = db.GetCollection<Champion>("championKolekcija");

            var filterChamp = Builders<Champion>.Filter.Eq("name", tbxChampname.Text);
            var resultChamp = Champ_collection.Find(filterChamp).ToList();

            bool exist = false;

            foreach (Champion champ in resultChamp)
            {
                if (resultChamp.Count > 0)
                {
                    exist = true;
                }
            }
            


            if (tbxChampname.Text != "" && exist == false)
            {
                Champion champ = new Champion();
                champ.name = tbxChampname.Text;
                champ.role = cbxRole.Text;
                champ.prefferedLane = cbxPrefLane.Text;
                champ.magicResource = cbxMagicResource.Text;
                champ.baseStats.Add(tbxhp.Text);
                champ.baseStats.Add(tbxad.Text);
                champ.baseStats.Add(tbxas.Text);
                champ.baseStats.Add(tbxa.Text);
                champ.baseStats.Add(tbxm.Text);
                champ.baseStats.Add(tbxap.Text);
                champ.baseStats.Add(tbxms.Text);
                champ.baseStats.Add(tbxmr.Text);
                champ.strongAgainst = tbxStrong.Text;
                champ.weakAgainst = tbxWeak.Text;


                Champ_collection.InsertOne(champ);

                MessageBox.Show( "Created champion: " + champ.name);
                

            }
            else
            {
                MessageBox.Show("Cannot create champion without a name, or champion " +
                    "with that name already exists.");
            }
        }

      

        

        private void addItem_Click(object sender, EventArgs e)
        {
            if (tbxChampname.Text != "")
            {
                var client = new MongoClient();
                var db = client.GetDatabase("LolBaza");
                var Champ_collection = db.GetCollection<Champion>("championKolekcija");

                var filterChamp = Builders<Champion>.Filter.Eq("name", tbxChampname.Text);
                var resultChamp = Champ_collection.Find(filterChamp).ToList();
                bool exist=false;

                foreach(Champion champ in resultChamp)
                {
                    if(resultChamp.Count > 0)
                    {
                        exist = true;
                    }
                }

                if (exist)
                {
                    MessageBox.Show("You may add items to this champion.");
                    var AddItem = new Add_Item(tbxChampname.Text);
                    AddItem.Show();
                }
                else
                {
                    MessageBox.Show("Non existent champion!");
                }            
             }
            else
            {
                MessageBox.Show("Please provide a champion name first.");
            }
    }

        private void addRune_Click(object sender, EventArgs e)
        {
            if (tbxChampname.Text != "")
            {
                var client = new MongoClient();
                var db = client.GetDatabase("LolBaza");
                var Champ_collection = db.GetCollection<Champion>("championKolekcija");

                var filterChamp = Builders<Champion>.Filter.Eq("name", tbxChampname.Text);
                var resultChamp = Champ_collection.Find(filterChamp).ToList();
                bool exist = false;

                foreach (Champion champ in resultChamp)
                {
                    if (resultChamp.Count > 0)
                    {
                        exist = true;
                    }
                }
                if (exist)
                {
                    MessageBox.Show("You may add Rune Pages to this champion.");
                    var AddRune = new Add_Rune(tbxChampname.Text);
                    AddRune.Show();
                    
                }
                else
                {
                    MessageBox.Show("Non existent champion!");
                }
            }
            else
            {
                MessageBox.Show("Please provide a champion name first.");
            }
        }

        private void addAbility_Click(object sender, EventArgs e)
        {
            if (tbxChampname.Text != "")
            {
                var client = new MongoClient();
                var db = client.GetDatabase("LolBaza");
                var Champ_collection = db.GetCollection<Champion>("championKolekcija");

                var filterChamp = Builders<Champion>.Filter.Eq("name", tbxChampname.Text);
                var resultChamp = Champ_collection.Find(filterChamp).ToList();
                bool exist = false;

                foreach (Champion champ in resultChamp)
                {
                    if (resultChamp.Count > 0)
                    {
                        exist = true;
                    }
                }
                if (exist)
                {
                    MessageBox.Show("You may add Abilities to this champion.");
                    var AddAbility = new Add_Ability(tbxChampname.Text);
                    AddAbility.Show();
                }
                else
                {
                    MessageBox.Show("Non existent champion!");
                }

            }
            else
            {
                MessageBox.Show("Please provide a champion name first.");
            }

        }
    }
}
