﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace mongodbaplikacija
{
    public partial class RuneForma : Form
    {
        public RuneForma()
        {
            InitializeComponent();
        }

        private void crt_rune_Click(object sender, EventArgs e)
        {

            var client = new MongoClient();

            var db = client.GetDatabase("LolBaza");
            //db.DropCollection("runeKolekcija");
            var collectionRune = db.GetCollection<RunePage>("runeKolekcija");

            string secondarySlot1 = "";
            string secondarySlot2 = ""; 
            if (comboSecondarySlot1.Enabled && comboSecondarySlot2.Enabled)
            {
                secondarySlot1 = comboSecondarySlot1.Text;
                secondarySlot2 = comboSecondarySlot2.Text;
            }
            if (comboSecondarySlot1.Enabled && comboSecondarySlot3.Enabled)
            {
                secondarySlot1 = comboSecondarySlot1.Text;
                secondarySlot2 = comboSecondarySlot3.Text;

            }
            if (comboSecondarySlot2.Enabled && comboSecondarySlot3.Enabled)
            {
                secondarySlot1 = comboSecondarySlot2.Text;
                secondarySlot2 = comboSecondarySlot3.Text;
            }

            
            if (textBoxRuneName.Text != "")
            {
                RunePage rp1 = new RunePage();
                rp1.RuneName = textBoxRuneName.Text;
                rp1.PrimaryRunes = cbxPrimary.Text;
                rp1.Keystone = comboKeystone.Text;
                rp1.PrimarySlot1 = comboPrimarySlot1.Text;
                rp1.PrimarySlot2 = comboPrimarySlot2.Text;
                rp1.PrimarySlot3 = comboPrimarySlot3.Text;
                rp1.SecondaryRunes = cbxSecondary.Text;
                rp1.SecondarySlot1 = secondarySlot1;
                rp1.SecondarySlot2 = secondarySlot2;

                collectionRune.InsertOne(rp1);
                MessageBox.Show("Rune Page Created Successfully");
            }
            else
            {
                MessageBox.Show("Enter Page name first!");
            }
            
        }

        private void cbxPrimary_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("promenjena primary stavka");

            if (cbxPrimary.SelectedItem.Equals("Precision"))
            {
                comboKeystone.Items.Clear();
                comboKeystone.Text = "";
                // MessageBox.Show("Selektovano precision");
                comboKeystone.Items.Add("Press the Attack");
                comboKeystone.Items.Add("Lethal Tempo");
                comboKeystone.Items.Add("Fleet Footwork");
                comboKeystone.Items.Add("Conqueror");

                comboPrimarySlot1.Items.Clear();
                comboPrimarySlot1.Text = "";
                comboPrimarySlot1.Items.Add("Overheal");
                comboPrimarySlot1.Items.Add("Triumph");
                comboPrimarySlot1.Items.Add("Presence of Mind");

                comboPrimarySlot2.Items.Clear();
                comboPrimarySlot2.Text = "";
                comboPrimarySlot2.Items.Add("Legend: Alacrity");
                comboPrimarySlot2.Items.Add("Legend: Tenacity");
                comboPrimarySlot2.Items.Add("Legend: Bloodline");

                comboPrimarySlot3.Items.Clear();
                comboPrimarySlot3.Text = "";
                comboPrimarySlot3.Items.Add("Coup de Grace");
                comboPrimarySlot3.Items.Add("Cut Down");
                comboPrimarySlot3.Items.Add("Last Stand");

            }
            else if (cbxPrimary.SelectedItem.Equals("Domination"))
            {
                comboKeystone.Items.Clear();
                comboKeystone.Text = "";
                // MessageBox.Show("Selektovano Domination");
                comboKeystone.Items.Add("Electrocute");
                comboKeystone.Items.Add("Predator");
                comboKeystone.Items.Add("Dark Harvest");
                comboKeystone.Items.Add("Hail of blades");

                comboPrimarySlot1.Items.Clear();
                comboPrimarySlot1.Text = "";
                comboPrimarySlot1.Items.Add("Cheap Shot");
                comboPrimarySlot1.Items.Add("Taste of Blood");
                comboPrimarySlot1.Items.Add("Sudden Impact");

                comboPrimarySlot2.Items.Clear();
                comboPrimarySlot2.Text = "";
                comboPrimarySlot2.Items.Add("Zombie Ward");
                comboPrimarySlot2.Items.Add("Ghost Poro");
                comboPrimarySlot2.Items.Add("Eyeball Collection");

                comboPrimarySlot3.Items.Clear();
                comboPrimarySlot3.Text = "";
                comboPrimarySlot3.Items.Add("Ravenous Hunter");
                comboPrimarySlot3.Items.Add("Ingenious Hunter");
                comboPrimarySlot3.Items.Add("Relentless Hunter");
                comboPrimarySlot3.Items.Add("Ultimate Hunter");
            }
            else if (cbxPrimary.SelectedItem.Equals("Sorcery"))
            {
                comboKeystone.Items.Clear();
                comboKeystone.Text = "";
                comboKeystone.Items.Add("Summon Aery");
                comboKeystone.Items.Add("Arcane Comet");
                comboKeystone.Items.Add("Phase Rush");

                comboPrimarySlot1.Items.Clear();
                comboPrimarySlot1.Text = "";
                comboPrimarySlot1.Items.Add("Nullifying Orb");
                comboPrimarySlot1.Items.Add("Manaflow Band");
                comboPrimarySlot1.Items.Add("Nimbus Cloak");

                comboPrimarySlot2.Items.Clear();
                comboPrimarySlot2.Text = "";
                comboPrimarySlot2.Items.Add("Transcendence");
                comboPrimarySlot2.Items.Add("Celerity");
                comboPrimarySlot2.Items.Add("Absolute Focus");

                comboPrimarySlot3.Items.Clear();
                comboPrimarySlot3.Text = "";
                comboPrimarySlot3.Items.Add("Scorch");
                comboPrimarySlot3.Items.Add("Water Walking");
                comboPrimarySlot3.Items.Add("Gathering Storm");

            }
            else if (cbxPrimary.SelectedItem.Equals("Resolve"))
            {
                comboKeystone.Items.Clear();
                comboKeystone.Text = "";
                comboKeystone.Items.Add("Grasp of the Undying");
                comboKeystone.Items.Add("Aftershock");
                comboKeystone.Items.Add("Guardian");

                comboPrimarySlot1.Items.Clear();
                comboPrimarySlot1.Text = "";
                comboPrimarySlot1.Items.Add("Demolish");
                comboPrimarySlot1.Items.Add("Font of Life");
                comboPrimarySlot1.Items.Add("Shield Base");

                comboPrimarySlot2.Items.Clear();
                comboPrimarySlot2.Text = "";
                comboPrimarySlot2.Items.Add("Conditioning");
                comboPrimarySlot2.Items.Add("Second Wind");
                comboPrimarySlot2.Items.Add("Bone Plating");

                comboPrimarySlot3.Items.Clear();
                comboPrimarySlot3.Text = "";
                comboPrimarySlot3.Items.Add("Overgrowth");
                comboPrimarySlot3.Items.Add("Revitalize");
                comboPrimarySlot3.Items.Add("Unflinching");
            }
            else if (cbxPrimary.SelectedItem.Equals("Inspiration"))
            {
                comboKeystone.Items.Clear();
                comboKeystone.Text = "";
                comboKeystone.Items.Add("Glacial Augment");
                comboKeystone.Items.Add("Kleptomancy");
                comboKeystone.Items.Add("Unsealed Spellbook");

                comboPrimarySlot1.Items.Clear();
                comboPrimarySlot1.Text = "";
                comboPrimarySlot1.Items.Add("Hextech Flashtraption");
                comboPrimarySlot1.Items.Add("Magical Footwear");
                comboPrimarySlot1.Items.Add("Perfect Timing");

                comboPrimarySlot2.Items.Clear();
                comboPrimarySlot2.Text = "";
                comboPrimarySlot2.Items.Add("Future's Market");
                comboPrimarySlot2.Items.Add("Minion Dematerializer");
                comboPrimarySlot2.Items.Add("Biscuit Delivery");

                comboPrimarySlot3.Items.Clear();
                comboPrimarySlot3.Text = "";
                comboPrimarySlot3.Items.Add("Cosmic Insight");
                comboPrimarySlot3.Items.Add("Approach Velocity");
                comboPrimarySlot3.Items.Add("Time Warp Tonic");
            }
            
        }

        

        private void cbxSecondary_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxSecondary.SelectedItem.Equals("Precision") && !cbxPrimary.SelectedItem.Equals("Precision"))
            {
                comboSecondarySlot1.Items.Clear();
                comboSecondarySlot1.Text = "";
                comboSecondarySlot1.Items.Add("Overheal");
                comboSecondarySlot1.Items.Add("Triumph");
                comboSecondarySlot1.Items.Add("Presence of Mind");

                comboSecondarySlot2.Items.Clear();
                comboSecondarySlot2.Text = "";
                comboSecondarySlot2.Items.Add("Legend: Alacrity");
                comboSecondarySlot2.Items.Add("Legend: Tenacity");
                comboSecondarySlot2.Items.Add("Legend: Bloodline");

                comboSecondarySlot3.Items.Clear();
                comboSecondarySlot3.Text = "";
                comboSecondarySlot3.Items.Add("Coup de Grace");
                comboSecondarySlot3.Items.Add("Cut Down");
                comboSecondarySlot3.Items.Add("Last Stand");

            }
            else if (cbxSecondary.SelectedItem.Equals("Domination") && !cbxPrimary.SelectedItem.Equals("Domination"))
            {

                comboSecondarySlot1.Items.Clear();
                comboSecondarySlot1.Text = "";
                comboSecondarySlot1.Items.Add("Cheap Shot");
                comboSecondarySlot1.Items.Add("Taste of Blood");
                comboSecondarySlot1.Items.Add("Sudden Impact");

                comboSecondarySlot2.Items.Clear();
                comboSecondarySlot2.Text = "";
                comboSecondarySlot2.Items.Add("Zombie Ward");
                comboSecondarySlot2.Items.Add("Ghost Poro");
                comboSecondarySlot2.Items.Add("Eyeball Collection");

                comboSecondarySlot3.Items.Clear();
                comboSecondarySlot3.Text = "";
                comboSecondarySlot3.Items.Add("Ravenous Hunter");
                comboSecondarySlot3.Items.Add("Ingenious Hunter");
                comboSecondarySlot3.Items.Add("Relentless Hunter");
                comboSecondarySlot3.Items.Add("Ultimate Hunter");
            }
            else if (cbxSecondary.SelectedItem.Equals("Sorcery") && !cbxPrimary.SelectedItem.Equals("Sorcery"))
            {
                comboSecondarySlot1.Items.Clear();
                comboSecondarySlot1.Text = "";
                comboSecondarySlot1.Items.Add("Nullifying Orb");
                comboSecondarySlot1.Items.Add("Manaflow Band");
                comboSecondarySlot1.Items.Add("Nimbus Cloak");

                comboSecondarySlot2.Items.Clear();
                comboSecondarySlot2.Text = "";
                comboSecondarySlot2.Items.Add("Transcendence");
                comboSecondarySlot2.Items.Add("Celerity");
                comboSecondarySlot2.Items.Add("Absolute Focus");

                comboSecondarySlot3.Items.Clear();
                comboSecondarySlot3.Text = "";
                comboSecondarySlot3.Items.Add("Scorch");
                comboSecondarySlot3.Items.Add("Water Walking");
                comboSecondarySlot3.Items.Add("Gathering Storm");

            }
            else if (cbxSecondary.SelectedItem.Equals("Resolve") && !cbxPrimary.SelectedItem.Equals("Resolve"))
            {
                comboSecondarySlot1.Items.Clear();
                comboSecondarySlot1.Text = "";
                comboSecondarySlot1.Items.Add("Demolish");
                comboSecondarySlot1.Items.Add("Font of Life");
                comboSecondarySlot1.Items.Add("Shield Base");

                comboSecondarySlot2.Items.Clear();
                comboSecondarySlot2.Text = "";
                comboSecondarySlot2.Items.Add("Conditioning");
                comboSecondarySlot2.Items.Add("Second Wind");
                comboSecondarySlot2.Items.Add("Bone Plating");

                comboSecondarySlot3.Items.Clear();
                comboSecondarySlot3.Text = "";
                comboSecondarySlot3.Items.Add("Overgrowth");
                comboSecondarySlot3.Items.Add("Revitalize");
                comboSecondarySlot3.Items.Add("Unflinching");
            }
            else if (cbxSecondary.SelectedItem.Equals("Inspiration") && !cbxPrimary.SelectedItem.Equals("Inspiration"))
            {
                comboSecondarySlot1.Items.Clear();
                comboSecondarySlot1.Text = "";
                comboSecondarySlot1.Items.Add("Hextech Flashtraption");
                comboSecondarySlot1.Items.Add("Magical Footwear");
                comboSecondarySlot1.Items.Add("Perfect Timing");

                comboSecondarySlot2.Items.Clear();
                comboSecondarySlot2.Text = "";
                comboSecondarySlot2.Items.Add("Future's Market");
                comboSecondarySlot2.Items.Add("Minion Dematerializer");
                comboSecondarySlot2.Items.Add("Biscuit Delivery");

                comboSecondarySlot3.Items.Clear();
                comboSecondarySlot3.Text = "";
                comboSecondarySlot3.Items.Add("Cosmic Insight");
                comboSecondarySlot3.Items.Add("Approach Velocity");
                comboSecondarySlot3.Items.Add("Time Warp Tonic");
            }
            else MessageBox.Show("Morate selektovati drugi path za Secondary Runes");
        }

        private void buttonViewRunes_Click(object sender, EventArgs e)
        {
           // var viewRunesForma = new ViewRunesForma();
           // viewRunesForma.Show();

            var client = new MongoClient();

            var db = client.GetDatabase("LolBaza");
            //db.DropCollection("runeKolekcija");
            var collectionRune = db.GetCollection<RunePage>("runeKolekcija");

            dataGridRunes.Rows.Clear();
            foreach (RunePage rune in collectionRune.Find(new BsonDocument()).ToList())
            {
                

                dataGridRunes.Rows.Insert(dataGridRunes.Rows.Count, new object[] {rune.RuneName, rune.PrimaryRunes, rune.Keystone, rune.PrimarySlot1,
                        rune.PrimarySlot2, rune.PrimarySlot3, rune.SecondaryRunes, rune.SecondarySlot1, rune.SecondarySlot2});
            }
        }

        private void checkIzaberi1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkIzaberi1.Checked == true && checkIzaberi2.Checked == true)
            {

                comboSecondarySlot3.Enabled = false;
            }
            else
            {
                comboSecondarySlot3.Enabled = true;
            }
            if (checkIzaberi1.Checked == true && checkIzaberi3.Checked == true)
            {
                comboSecondarySlot2.Enabled = false;
            }
            else
            {
                comboSecondarySlot2.Enabled = true;
            }
        }

        private void checkIzaberi2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkIzaberi2.Checked == true && checkIzaberi1.Checked == true)
            {

                comboSecondarySlot3.Enabled = false;
            }
            else
            {
                comboSecondarySlot3.Enabled = true;
            }
            if (checkIzaberi2.Checked == true && checkIzaberi3.Checked == true)
            {
                comboSecondarySlot1.Enabled = false;
            }
            else
            {
                comboSecondarySlot1.Enabled = true;
            }
        }

        private void checkIzaberi3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkIzaberi3.Checked == true && checkIzaberi1.Checked == true)
            {

                comboSecondarySlot2.Enabled = false;
            }
            else
            {
                comboSecondarySlot2.Enabled = true;
            }
            if (checkIzaberi3.Checked == true && checkIzaberi2.Checked == true)
            {
                comboSecondarySlot1.Enabled = false;
            }
            else
            {
                comboSecondarySlot1.Enabled = true;
            }
        }

        private void btnDeleteRune_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();

            var db = client.GetDatabase("LolBaza");
            //db.DropCollection("runeKolekcija");
            var collectionRune = db.GetCollection<RunePage>("runeKolekcija");


            string toDel = "You didn't select the correct rune!";
            if (dataGridRunes.SelectedRows.Count != 0)
            {
                toDel = dataGridRunes.SelectedCells[0].Value.ToString();

                var filter = Builders<RunePage>.Filter.And(
                  Builders<RunePage>.Filter.Eq("PrimaryRunes", dataGridRunes.SelectedCells[1].Value.ToString()),
                  Builders<RunePage>.Filter.Eq("Keystone", dataGridRunes.SelectedCells[2].Value.ToString()),
                  Builders<RunePage>.Filter.Eq("SecondaryRunes", dataGridRunes.SelectedCells[6].Value.ToString())
                  );

                collectionRune.DeleteOne(filter);
                MessageBox.Show("Deleted one rune: " + toDel);

                dataGridRunes.Rows.Clear();
                foreach (RunePage rune in collectionRune.Find(new BsonDocument()).ToList())
                {
                    dataGridRunes.Rows.Insert(dataGridRunes.Rows.Count, new object[] { rune.RuneName, rune.PrimaryRunes, rune.Keystone, rune.PrimarySlot1,
                        rune.PrimarySlot2, rune.PrimarySlot3, rune.SecondaryRunes, rune.SecondarySlot1, rune.SecondarySlot2});
                }

            }
            else
            {
                MessageBox.Show(toDel + " Please select the row to delete it.");
            }
        }

        private void btnSelectUpdateRune_Click(object sender, EventArgs e)
        {
            if (dataGridRunes.SelectedRows.Count != 0 && dataGridRunes.SelectedRows.Count < 2)
            {
                // MessageBox.Show(dgViewAbilities.SelectedCells[0].Value.ToString() + dgViewAbilities.SelectedCells[1].Value.ToString());
                textBoxRuneName.Text = dataGridRunes.SelectedCells[0].Value.ToString();
                cbxPrimary.Text = dataGridRunes.SelectedCells[1].Value.ToString();
                comboKeystone.Text = dataGridRunes.SelectedCells[2].Value.ToString();
                comboPrimarySlot1.Text = dataGridRunes.SelectedCells[3].Value.ToString();
                comboPrimarySlot2.Text = dataGridRunes.SelectedCells[4].Value.ToString();
                comboPrimarySlot3.Text = dataGridRunes.SelectedCells[5].Value.ToString();
                cbxSecondary.Text = dataGridRunes.SelectedCells[6].Value.ToString();
                comboSecondarySlot1.Text = dataGridRunes.SelectedCells[7].Value.ToString();
                comboSecondarySlot2.Text = dataGridRunes.SelectedCells[8].Value.ToString();
                checkIzaberi1.Checked = false;
                checkIzaberi2.Checked = false;
                checkIzaberi3.Checked = false;

                MessageBox.Show("Selected row for update: " + dataGridRunes.SelectedRows[0].Index.ToString());
            }
            else
                MessageBox.Show("Select specified row for update!");

        }

        private void btnUpdateRune_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collection = db.GetCollection<RunePage>("runeKolekcija");


            var filter = Builders<RunePage>.Filter.Eq("RuneName", textBoxRuneName.Text);
            var result = collection.Find(filter).ToList();
            var updateDef = Builders<RunePage>.Update.Set(i=> i.RuneName, textBoxRuneName.Text);
            if (checkIzaberi1.Checked && checkIzaberi2.Checked)
            {
                updateDef = Builders<RunePage>.Update.Set(i => i.RuneName, textBoxRuneName.Text).Set(i => i.PrimaryRunes, cbxPrimary.Text).Set(i => i.Keystone, comboKeystone.Text).Set(
                    i => i.PrimarySlot1, comboPrimarySlot1.Text).Set(i => i.PrimarySlot2, comboPrimarySlot2.Text).Set(i => i.PrimarySlot3, comboPrimarySlot3.Text).Set(
                    i => i.SecondaryRunes, cbxSecondary.Text).Set(i => i.SecondarySlot1, comboSecondarySlot1.Text).Set(i => i.SecondarySlot2, comboSecondarySlot2.Text);
            }
            else if(checkIzaberi1.Checked && checkIzaberi3.Checked)
            {
                updateDef = Builders<RunePage>.Update.Set(i => i.RuneName, textBoxRuneName.Text).Set(i => i.PrimaryRunes, cbxPrimary.Text).Set(i => i.Keystone, comboKeystone.Text).Set(
                   i => i.PrimarySlot1, comboPrimarySlot1.Text).Set(i => i.PrimarySlot2, comboPrimarySlot2.Text).Set(i => i.PrimarySlot3, comboPrimarySlot3.Text).Set(
                   i => i.SecondaryRunes, cbxSecondary.Text).Set(i => i.SecondarySlot1, comboSecondarySlot1.Text).Set(i => i.SecondarySlot2, comboSecondarySlot3.Text);
            }
            else if(checkIzaberi2.Checked && checkIzaberi3.Checked)
            {
                updateDef = Builders<RunePage>.Update.Set(i => i.RuneName, textBoxRuneName.Text).Set(i => i.PrimaryRunes, cbxPrimary.Text).Set(i => i.Keystone, comboKeystone.Text).Set(
                   i => i.PrimarySlot1, comboPrimarySlot1.Text).Set(i => i.PrimarySlot2, comboPrimarySlot2.Text).Set(i => i.PrimarySlot3, comboPrimarySlot3.Text).Set(
                   i => i.SecondaryRunes, cbxSecondary.Text).Set(i => i.SecondarySlot1, comboSecondarySlot2.Text).Set(i => i.SecondarySlot2, comboSecondarySlot3.Text);
            }
            collection.UpdateOne(filter, updateDef);

            if (result.Count != 0)
            {
                foreach (RunePage rune in result)
                {
                    MessageBox.Show("Updated item: " + rune.RuneName);
                }
            }
            else
            {
                MessageBox.Show("Name field is not optional. You can't change it!");
            }

            dataGridRunes.Rows.Clear();
            foreach (RunePage rune in collection.Find(new BsonDocument()).ToList())
            {
                dataGridRunes.Rows.Insert(dataGridRunes.Rows.Count, new object[] { rune.RuneName, rune.PrimaryRunes, rune.Keystone, rune.PrimarySlot1,
                        rune.PrimarySlot2, rune.PrimarySlot3, rune.SecondaryRunes, rune.SecondarySlot1, rune.SecondarySlot2});
            }


        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dataGridRunes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
