﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace mongodbaplikacija
{
    public partial class Add_Rune : Form
    {
        public Add_Rune(string champ)
        {
            InitializeComponent();
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            lblChampName.Text = champ;
            var collectionRune = db.GetCollection<RunePage>("runeKolekcija");

            dataGridRunes.Rows.Clear();
            foreach (RunePage rune in collectionRune.Find(new BsonDocument()).ToList())
            {


                dataGridRunes.Rows.Insert(dataGridRunes.Rows.Count, new object[] {rune.RuneName, rune.PrimaryRunes, rune.Keystone, rune.PrimarySlot1,
                        rune.PrimarySlot2, rune.PrimarySlot3, rune.SecondaryRunes, rune.SecondarySlot1, rune.SecondarySlot2});
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");

            var Rune_collection = db.GetCollection<RunePage>("runeKolekcija");

            var Champ_collection = db.GetCollection<Champion>("championKolekcija");
            var rune_Name = dataGridRunes.SelectedCells[0].Value.ToString();
            var filterRune = Builders<RunePage>.Filter.Eq("RuneName", rune_Name);
            var resultRune = Rune_collection.Find(filterRune).ToList();

            var filterChamp = Builders<Champion>.Filter.Eq("name", lblChampName.Text);
            var resultChamp = Champ_collection.Find(filterChamp).ToList();

            if (resultRune.Count != 0 && resultChamp.Count != 0)
            {
                var champ_and_runes = resultRune.Zip(resultChamp, (r, c) => new { resultRune = r, resultChamp = c });
                foreach (var rc in champ_and_runes)
                {

                    rc.resultChamp.runes.Add(rc.resultRune);
                    rc.resultRune.champions.Add(rc.resultChamp.name);
                    var updateDef = Builders<RunePage>.Update.Set(i => i.champions, rc.resultRune.champions);
                    Rune_collection.UpdateOne(filterRune, updateDef);
                    var updateDefChamp = Builders<Champion>.Update.Set(c => c.runes, rc.resultChamp.runes);
                    Champ_collection.UpdateOne(filterChamp, updateDefChamp);
                    MessageBox.Show("Added Rune Page :" + rc.resultRune.RuneName + ", to " + rc.resultChamp.name + "'s Rune Pages.");
                }
            }
            else
            {
                MessageBox.Show("Something went wrong, try again.");
            }
        }
    }
}
