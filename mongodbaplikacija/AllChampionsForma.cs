﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace mongodbaplikacija
{
    public partial class AllChampionsForma : Form
    {
        public AllChampionsForma()
        {
            InitializeComponent();
            
        }

        private void AllChampionsForma_Load(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collection = db.GetCollection<Champion>("championKolekcija");
            foreach (Champion champs in collection.Find(new BsonDocument()).ToList())
            {
                var index = dgvChampions.Rows.Add();
                dgvChampions.Rows[index].Cells["name"].Value = champs.name;
                dgvChampions.Rows[index].Cells["role"].Value = champs.role;
                dgvChampions.Rows[index].Cells["prefLane"].Value = champs.prefferedLane;
                dgvChampions.Rows[index].Cells["mresource"].Value = champs.magicResource;
                dgvChampions.Rows[index].Cells["strong"].Value = champs.strongAgainst;
                dgvChampions.Rows[index].Cells["weak"].Value = champs.weakAgainst;
                if (champs.baseStats.Count() > 1)
                {
                    string baseStats = string.Join(",", champs.baseStats.ToArray());
                    dgvChampions.Rows[index].Cells["stats"].Value = baseStats;
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collection = db.GetCollection<Item>("championKolekcija");
            string toDel = "You didn't select the correct item!";
            if (dgvChampions.SelectedCells[0].Value != null)
            {
                toDel = dgvChampions.SelectedCells[0].Value.ToString();

                var filter = Builders<Item>.Filter.Eq("name", toDel);

                collection.DeleteOne(filter);
                MessageBox.Show("Deleted one item: " + toDel);
            }
            else
            {
                MessageBox.Show(toDel + " Please select the item name to delete it.");
            }
        }

        private void AllChampionsForma_Activated(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collection = db.GetCollection<Champion>("championKolekcija");
            dgvChampions.Rows.Clear();
            foreach (Champion champs in collection.Find(new BsonDocument()).ToList())
            {
                var index = dgvChampions.Rows.Add();
                dgvChampions.Rows[index].Cells["name"].Value = champs.name;
                dgvChampions.Rows[index].Cells["role"].Value = champs.role;
                dgvChampions.Rows[index].Cells["prefLane"].Value = champs.prefferedLane;
                dgvChampions.Rows[index].Cells["mresource"].Value = champs.magicResource;
                dgvChampions.Rows[index].Cells["strong"].Value = champs.strongAgainst;
                dgvChampions.Rows[index].Cells["weak"].Value = champs.weakAgainst;
                if (champs.baseStats.Count() > 1)
                {
                    string baseStats = string.Join(",", champs.baseStats.ToArray());
                    dgvChampions.Rows[index].Cells["stats"].Value = baseStats;
                }
            }
        }

        private void viewItems_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collectionItem = db.GetCollection<Item>("ItemKolekcija");
            dgvItems.Rows.Clear();

            string champItems = "Select champion";
            if (dgvChampions.SelectedCells[0].Value != null)
            {
                champItems = dgvChampions.SelectedCells[0].Value.ToString();

                var filter = Builders<Item>.Filter.Eq("champions", champItems);
                var resultChamp = collectionItem.Find(filter).ToList();
                foreach (Item item in resultChamp.ToList())
                {
                    string itemType = string.Join(",", item.itemType.ToArray());
                    dgvItems.Rows.Insert(dgvItems.Rows.Count, new object[] { item.itemName, itemType, item.itemCost, item.itemDescription });
                }
            }
            else
            {
                MessageBox.Show("Invalid champion.");
            }

            
        }

        private void viewRune_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collectionRune = db.GetCollection<RunePage>("runeKolekcija");
            dataGridRunes.Rows.Clear();

            string champRunes = "Select champion";
            if (dgvChampions.SelectedCells[0].Value != null)
            {
                champRunes = dgvChampions.SelectedCells[0].Value.ToString();

                var filterRune = Builders<RunePage>.Filter.Eq("champions", champRunes);
                var resultChamp = collectionRune.Find(filterRune).ToList();
                foreach (RunePage rune in resultChamp.ToList())
                {
                    dataGridRunes.Rows.Insert(dataGridRunes.Rows.Count, new object[] {rune.RuneName, rune.PrimaryRunes, rune.Keystone, rune.PrimarySlot1,
                        rune.PrimarySlot2, rune.PrimarySlot3, rune.SecondaryRunes, rune.SecondarySlot1, rune.SecondarySlot2});
                }
            }
            else
            {
                MessageBox.Show("Invalid champion.");
            }

        }

        private void viewAbilities_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collectionAbility = db.GetCollection<Ability>("abilityKolekcija");
            dgViewAbilities.Rows.Clear();

            string champAbilities = "Select champion";
            if (dgvChampions.SelectedCells[0].Value != null)
            {
                champAbilities = dgvChampions.SelectedCells[0].Value.ToString();

                var filterAbility = Builders<Ability>.Filter.Eq("champions", champAbilities);
                var resultChamp = collectionAbility.Find(filterAbility).ToList();
                foreach (Ability ability in resultChamp.ToList())
                {
                    var passive = "";
                    if (ability.PassiveAbility)
                        passive = "Passive";
                    else
                        passive = "Not passive";

                    dgViewAbilities.Rows.Insert(dgViewAbilities.Rows.Count, new object[] {ability.AbilityName, passive,
                    ability.AbilityShortcut ,  ability.AbilityRank, ability.AbilityCost, ability.AbilityDescription });
                }
            }
            else
            {
                MessageBox.Show("Invalid champion.");
            }

        }

        private void btnViewAbilities_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collectionAbility = db.GetCollection<Ability>("abilityKolekcija");
            dgViewAbilities.Rows.Clear();

            string champAbilities = "Select champion";
            if (dgvChampions.SelectedCells[0].Value != null)
            {
                champAbilities = dgvChampions.SelectedCells[0].Value.ToString();

                var filterAbility = Builders<Ability>.Filter.Eq("champions", champAbilities);
                var resultChamp = collectionAbility.Find(filterAbility).ToList();
                foreach (Ability ability in resultChamp.ToList())
                {
                    var passive = "";
                    if (ability.PassiveAbility)
                        passive = "Passive";
                    else
                        passive = "Not passive";

                    dgViewAbilities.Rows.Insert(dgViewAbilities.Rows.Count, new object[] {ability.AbilityName, passive,
                    ability.AbilityShortcut ,  ability.AbilityRank, ability.AbilityCost, ability.AbilityDescription });
                }
            }
            else
            {
                MessageBox.Show("Invalid champion.");
            }
        }
    }
}
