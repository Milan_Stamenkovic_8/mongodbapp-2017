﻿namespace mongodbaplikacija
{
    partial class ItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemForm));
            this.tbxItemName = new System.Windows.Forms.TextBox();
            this.lbxItemName = new System.Windows.Forms.Label();
            this.gbxItemType = new System.Windows.Forms.GroupBox();
            this.cbxMovement = new System.Windows.Forms.CheckBox();
            this.cbxMagic = new System.Windows.Forms.CheckBox();
            this.cbxAttack = new System.Windows.Forms.CheckBox();
            this.cbxDefense = new System.Windows.Forms.CheckBox();
            this.cbxTools = new System.Windows.Forms.CheckBox();
            this.cbxStartingItems = new System.Windows.Forms.CheckBox();
            this.lbxItemCost = new System.Windows.Forms.Label();
            this.nudItemCost = new System.Windows.Forms.NumericUpDown();
            this.btnCreate = new System.Windows.Forms.Button();
            this.lblItemDescript = new System.Windows.Forms.Label();
            this.tbxitemDescription = new System.Windows.Forms.TextBox();
            this.btnView = new System.Windows.Forms.Button();
            this.gbxItemType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudItemCost)).BeginInit();
            this.SuspendLayout();
            // 
            // tbxItemName
            // 
            this.tbxItemName.Location = new System.Drawing.Point(156, 23);
            this.tbxItemName.Name = "tbxItemName";
            this.tbxItemName.Size = new System.Drawing.Size(370, 20);
            this.tbxItemName.TabIndex = 0;
            // 
            // lbxItemName
            // 
            this.lbxItemName.AutoSize = true;
            this.lbxItemName.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxItemName.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lbxItemName.Location = new System.Drawing.Point(20, 22);
            this.lbxItemName.Name = "lbxItemName";
            this.lbxItemName.Size = new System.Drawing.Size(109, 19);
            this.lbxItemName.TabIndex = 1;
            this.lbxItemName.Text = "Item Name:";
            // 
            // gbxItemType
            // 
            this.gbxItemType.Controls.Add(this.cbxMovement);
            this.gbxItemType.Controls.Add(this.cbxMagic);
            this.gbxItemType.Controls.Add(this.cbxAttack);
            this.gbxItemType.Controls.Add(this.cbxDefense);
            this.gbxItemType.Controls.Add(this.cbxTools);
            this.gbxItemType.Controls.Add(this.cbxStartingItems);
            this.gbxItemType.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxItemType.ForeColor = System.Drawing.SystemColors.Highlight;
            this.gbxItemType.Location = new System.Drawing.Point(12, 62);
            this.gbxItemType.Name = "gbxItemType";
            this.gbxItemType.Size = new System.Drawing.Size(514, 100);
            this.gbxItemType.TabIndex = 2;
            this.gbxItemType.TabStop = false;
            this.gbxItemType.Text = "Item Type";
            // 
            // cbxMovement
            // 
            this.cbxMovement.AutoSize = true;
            this.cbxMovement.ForeColor = System.Drawing.Color.Red;
            this.cbxMovement.Location = new System.Drawing.Point(351, 71);
            this.cbxMovement.Name = "cbxMovement";
            this.cbxMovement.Size = new System.Drawing.Size(118, 23);
            this.cbxMovement.TabIndex = 5;
            this.cbxMovement.Text = "Movement";
            this.cbxMovement.UseVisualStyleBackColor = true;
            // 
            // cbxMagic
            // 
            this.cbxMagic.AutoSize = true;
            this.cbxMagic.ForeColor = System.Drawing.Color.Red;
            this.cbxMagic.Location = new System.Drawing.Point(192, 71);
            this.cbxMagic.Name = "cbxMagic";
            this.cbxMagic.Size = new System.Drawing.Size(81, 23);
            this.cbxMagic.TabIndex = 4;
            this.cbxMagic.Text = "Magic";
            this.cbxMagic.UseVisualStyleBackColor = true;
            // 
            // cbxAttack
            // 
            this.cbxAttack.AutoSize = true;
            this.cbxAttack.ForeColor = System.Drawing.Color.Red;
            this.cbxAttack.Location = new System.Drawing.Point(13, 71);
            this.cbxAttack.Name = "cbxAttack";
            this.cbxAttack.Size = new System.Drawing.Size(91, 23);
            this.cbxAttack.TabIndex = 3;
            this.cbxAttack.Text = "Attack";
            this.cbxAttack.UseVisualStyleBackColor = true;
            // 
            // cbxDefense
            // 
            this.cbxDefense.AutoSize = true;
            this.cbxDefense.ForeColor = System.Drawing.Color.Red;
            this.cbxDefense.Location = new System.Drawing.Point(350, 20);
            this.cbxDefense.Name = "cbxDefense";
            this.cbxDefense.Size = new System.Drawing.Size(96, 23);
            this.cbxDefense.TabIndex = 2;
            this.cbxDefense.Text = "Defense";
            this.cbxDefense.UseVisualStyleBackColor = true;
            // 
            // cbxTools
            // 
            this.cbxTools.AutoSize = true;
            this.cbxTools.ForeColor = System.Drawing.Color.Red;
            this.cbxTools.Location = new System.Drawing.Point(192, 20);
            this.cbxTools.Name = "cbxTools";
            this.cbxTools.Size = new System.Drawing.Size(72, 23);
            this.cbxTools.TabIndex = 1;
            this.cbxTools.Text = "Tools";
            this.cbxTools.UseVisualStyleBackColor = true;
            // 
            // cbxStartingItems
            // 
            this.cbxStartingItems.AutoSize = true;
            this.cbxStartingItems.ForeColor = System.Drawing.Color.Red;
            this.cbxStartingItems.Location = new System.Drawing.Point(13, 24);
            this.cbxStartingItems.Name = "cbxStartingItems";
            this.cbxStartingItems.Size = new System.Drawing.Size(163, 23);
            this.cbxStartingItems.TabIndex = 0;
            this.cbxStartingItems.Text = "Starting Items";
            this.cbxStartingItems.UseVisualStyleBackColor = true;
            // 
            // lbxItemCost
            // 
            this.lbxItemCost.AutoSize = true;
            this.lbxItemCost.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxItemCost.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lbxItemCost.Location = new System.Drawing.Point(20, 284);
            this.lbxItemCost.Name = "lbxItemCost";
            this.lbxItemCost.Size = new System.Drawing.Size(101, 19);
            this.lbxItemCost.TabIndex = 3;
            this.lbxItemCost.Text = "Item Cost:";
            // 
            // nudItemCost
            // 
            this.nudItemCost.Increment = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.nudItemCost.Location = new System.Drawing.Point(156, 283);
            this.nudItemCost.Maximum = new decimal(new int[] {
            3700,
            0,
            0,
            0});
            this.nudItemCost.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudItemCost.Name = "nudItemCost";
            this.nudItemCost.Size = new System.Drawing.Size(120, 20);
            this.nudItemCost.TabIndex = 4;
            this.nudItemCost.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // btnCreate
            // 
            this.btnCreate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnCreate.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreate.ForeColor = System.Drawing.Color.Aqua;
            this.btnCreate.Location = new System.Drawing.Point(81, 335);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(164, 35);
            this.btnCreate.TabIndex = 5;
            this.btnCreate.Text = "Create Item";
            this.btnCreate.UseVisualStyleBackColor = false;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // lblItemDescript
            // 
            this.lblItemDescript.AutoSize = true;
            this.lblItemDescript.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemDescript.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblItemDescript.Location = new System.Drawing.Point(20, 178);
            this.lblItemDescript.Name = "lblItemDescript";
            this.lblItemDescript.Size = new System.Drawing.Size(162, 19);
            this.lblItemDescript.TabIndex = 6;
            this.lblItemDescript.Text = "Item Description:";
            // 
            // tbxitemDescription
            // 
            this.tbxitemDescription.Location = new System.Drawing.Point(188, 179);
            this.tbxitemDescription.Multiline = true;
            this.tbxitemDescription.Name = "tbxitemDescription";
            this.tbxitemDescription.Size = new System.Drawing.Size(338, 84);
            this.tbxitemDescription.TabIndex = 7;
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnView.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ForeColor = System.Drawing.Color.Blue;
            this.btnView.Location = new System.Drawing.Point(294, 335);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(164, 35);
            this.btnView.TabIndex = 8;
            this.btnView.Text = "View Items";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // ItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(543, 392);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.tbxitemDescription);
            this.Controls.Add(this.lblItemDescript);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.nudItemCost);
            this.Controls.Add(this.lbxItemCost);
            this.Controls.Add(this.gbxItemType);
            this.Controls.Add(this.lbxItemName);
            this.Controls.Add(this.tbxItemName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ItemForm";
            this.Text = "In Game Items";
            this.gbxItemType.ResumeLayout(false);
            this.gbxItemType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudItemCost)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxItemName;
        private System.Windows.Forms.Label lbxItemName;
        private System.Windows.Forms.GroupBox gbxItemType;
        private System.Windows.Forms.CheckBox cbxMovement;
        private System.Windows.Forms.CheckBox cbxMagic;
        private System.Windows.Forms.CheckBox cbxAttack;
        private System.Windows.Forms.CheckBox cbxDefense;
        private System.Windows.Forms.CheckBox cbxTools;
        private System.Windows.Forms.CheckBox cbxStartingItems;
        private System.Windows.Forms.Label lbxItemCost;
        private System.Windows.Forms.NumericUpDown nudItemCost;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label lblItemDescript;
        private System.Windows.Forms.TextBox tbxitemDescription;
        private System.Windows.Forms.Button btnView;
    }
}