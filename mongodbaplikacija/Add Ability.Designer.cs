﻿namespace mongodbaplikacija
{
    partial class Add_Ability
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Ability));
            this.label2 = new System.Windows.Forms.Label();
            this.lblChampName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvAbilities = new System.Windows.Forms.DataGridView();
            this.abilityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passiveAbility = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityShortcut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityRank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAdd = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbilities)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Current Champion:";
            // 
            // lblChampName
            // 
            this.lblChampName.AutoSize = true;
            this.lblChampName.Location = new System.Drawing.Point(177, 30);
            this.lblChampName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblChampName.Name = "lblChampName";
            this.lblChampName.Size = new System.Drawing.Size(0, 17);
            this.lblChampName.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 67);
            this.label1.Margin = new System.Windows.Forms.Padding(9, 0, 9, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(268, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Select item from the following list:";
            // 
            // dgvAbilities
            // 
            this.dgvAbilities.AllowUserToAddRows = false;
            this.dgvAbilities.BackgroundColor = System.Drawing.Color.Yellow;
            this.dgvAbilities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAbilities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.abilityName,
            this.passiveAbility,
            this.abilityShortcut,
            this.abilityRank,
            this.abilityCost,
            this.abilityDescription});
            this.dgvAbilities.Location = new System.Drawing.Point(18, 89);
            this.dgvAbilities.Margin = new System.Windows.Forms.Padding(9, 5, 9, 5);
            this.dgvAbilities.Name = "dgvAbilities";
            this.dgvAbilities.Size = new System.Drawing.Size(721, 286);
            this.dgvAbilities.TabIndex = 14;
            // 
            // abilityName
            // 
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Lime;
            this.abilityName.DefaultCellStyle = dataGridViewCellStyle3;
            this.abilityName.HeaderText = "Name";
            this.abilityName.Name = "abilityName";
            // 
            // passiveAbility
            // 
            this.passiveAbility.HeaderText = "Passive ability";
            this.passiveAbility.Name = "passiveAbility";
            // 
            // abilityShortcut
            // 
            this.abilityShortcut.HeaderText = "Shortcut";
            this.abilityShortcut.Name = "abilityShortcut";
            this.abilityShortcut.Width = 104;
            // 
            // abilityRank
            // 
            this.abilityRank.HeaderText = "Rank";
            this.abilityRank.Name = "abilityRank";
            // 
            // abilityCost
            // 
            this.abilityCost.HeaderText = "Cost";
            this.abilityCost.Name = "abilityCost";
            // 
            // abilityDescription
            // 
            this.abilityDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.abilityDescription.HeaderText = "Description";
            this.abilityDescription.Name = "abilityDescription";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Yellow;
            this.btnAdd.Location = new System.Drawing.Point(269, 404);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(174, 43);
            this.btnAdd.TabIndex = 15;
            this.btnAdd.Text = "Add Item";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // Add_Ability
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Yellow;
            this.ClientSize = new System.Drawing.Size(769, 460);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.dgvAbilities);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblChampName);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Ravie", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Add_Ability";
            this.Text = "Add Ability";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbilities)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblChampName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvAbilities;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn passiveAbility;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityShortcut;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityRank;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityDescription;
        private System.Windows.Forms.Button btnAdd;
    }
}