﻿namespace mongodbaplikacija
{
    partial class AllChampionsForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AllChampionsForma));
            this.dgvChampions = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.role = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prefLane = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mresource = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.strong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weak = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stats = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.Button();
            this.dgvItems = new System.Windows.Forms.DataGridView();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.viewItems = new System.Windows.Forms.Button();
            this.dataGridRunes = new System.Windows.Forms.DataGridView();
            this.RuneName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimaryRune = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Keystone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimarySlot1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimarySlot2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimarySlot3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecondaryRune = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecondarySlot1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecondarySlot2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.viewRune = new System.Windows.Forms.Button();
            this.dgViewAbilities = new System.Windows.Forms.DataGridView();
            this.abilityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passiveAbility = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityShortcut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityRank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abilityDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.viewAbilities = new System.Windows.Forms.Button();
            this.btnViewAbilities = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChampions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRunes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewAbilities)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvChampions
            // 
            this.dgvChampions.AllowUserToOrderColumns = true;
            this.dgvChampions.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dgvChampions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvChampions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvChampions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChampions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.role,
            this.prefLane,
            this.mresource,
            this.strong,
            this.weak,
            this.stats});
            this.dgvChampions.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvChampions.GridColor = System.Drawing.SystemColors.Control;
            this.dgvChampions.Location = new System.Drawing.Point(0, 0);
            this.dgvChampions.MultiSelect = false;
            this.dgvChampions.Name = "dgvChampions";
            this.dgvChampions.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvChampions.Size = new System.Drawing.Size(1183, 185);
            this.dgvChampions.TabIndex = 1;
            // 
            // name
            // 
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            this.name.Width = 140;
            // 
            // role
            // 
            this.role.HeaderText = "Role";
            this.role.Name = "role";
            this.role.Width = 130;
            // 
            // prefLane
            // 
            this.prefLane.HeaderText = "Preffered Lane";
            this.prefLane.Name = "prefLane";
            this.prefLane.Width = 120;
            // 
            // mresource
            // 
            this.mresource.HeaderText = "Magic Resource";
            this.mresource.Name = "mresource";
            this.mresource.Width = 110;
            // 
            // strong
            // 
            this.strong.HeaderText = "Strong Against";
            this.strong.Name = "strong";
            this.strong.Width = 140;
            // 
            // weak
            // 
            this.weak.HeaderText = "Weak Against";
            this.weak.Name = "weak";
            this.weak.Width = 140;
            // 
            // stats
            // 
            this.stats.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.stats.HeaderText = "Base Stats";
            this.stats.Name = "stats";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnDelete.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.Red;
            this.btnDelete.Location = new System.Drawing.Point(428, 202);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(161, 34);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Delete Champion";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // dgvItems
            // 
            this.dgvItems.AllowUserToAddRows = false;
            this.dgvItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvItems.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dgvItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemName,
            this.ItemType,
            this.ItemCost,
            this.itemDescription});
            this.dgvItems.GridColor = System.Drawing.SystemColors.Control;
            this.dgvItems.Location = new System.Drawing.Point(0, 242);
            this.dgvItems.MultiSelect = false;
            this.dgvItems.Name = "dgvItems";
            this.dgvItems.ReadOnly = true;
            this.dgvItems.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvItems.Size = new System.Drawing.Size(1183, 104);
            this.dgvItems.TabIndex = 3;
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // ItemType
            // 
            this.ItemType.HeaderText = "Type";
            this.ItemType.Name = "ItemType";
            this.ItemType.ReadOnly = true;
            // 
            // ItemCost
            // 
            this.ItemCost.HeaderText = "Cost";
            this.ItemCost.Name = "ItemCost";
            this.ItemCost.ReadOnly = true;
            // 
            // itemDescription
            // 
            this.itemDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.itemDescription.HeaderText = "Description";
            this.itemDescription.Name = "itemDescription";
            this.itemDescription.ReadOnly = true;
            // 
            // viewItems
            // 
            this.viewItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.viewItems.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewItems.ForeColor = System.Drawing.Color.Blue;
            this.viewItems.Location = new System.Drawing.Point(428, 362);
            this.viewItems.Name = "viewItems";
            this.viewItems.Size = new System.Drawing.Size(161, 30);
            this.viewItems.TabIndex = 4;
            this.viewItems.Text = "View Items";
            this.viewItems.UseVisualStyleBackColor = false;
            this.viewItems.Click += new System.EventHandler(this.viewItems_Click);
            // 
            // dataGridRunes
            // 
            this.dataGridRunes.AllowUserToAddRows = false;
            this.dataGridRunes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridRunes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dataGridRunes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRunes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RuneName,
            this.PrimaryRune,
            this.Keystone,
            this.PrimarySlot1,
            this.PrimarySlot2,
            this.PrimarySlot3,
            this.SecondaryRune,
            this.SecondarySlot1,
            this.SecondarySlot2,
            this.Column1});
            this.dataGridRunes.Location = new System.Drawing.Point(0, 399);
            this.dataGridRunes.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.dataGridRunes.Name = "dataGridRunes";
            this.dataGridRunes.Size = new System.Drawing.Size(1183, 98);
            this.dataGridRunes.TabIndex = 25;
            // 
            // RuneName
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RuneName.DefaultCellStyle = dataGridViewCellStyle3;
            this.RuneName.HeaderText = "Rune Name";
            this.RuneName.Name = "RuneName";
            // 
            // PrimaryRune
            // 
            this.PrimaryRune.HeaderText = "Primary rune";
            this.PrimaryRune.Name = "PrimaryRune";
            // 
            // Keystone
            // 
            this.Keystone.HeaderText = "Keystone";
            this.Keystone.Name = "Keystone";
            // 
            // PrimarySlot1
            // 
            this.PrimarySlot1.HeaderText = "Primary Slot1";
            this.PrimarySlot1.Name = "PrimarySlot1";
            // 
            // PrimarySlot2
            // 
            this.PrimarySlot2.HeaderText = "Primary Slot 2";
            this.PrimarySlot2.Name = "PrimarySlot2";
            // 
            // PrimarySlot3
            // 
            this.PrimarySlot3.HeaderText = "Primary Slot 3";
            this.PrimarySlot3.Name = "PrimarySlot3";
            // 
            // SecondaryRune
            // 
            this.SecondaryRune.HeaderText = "Secondary rune";
            this.SecondaryRune.Name = "SecondaryRune";
            // 
            // SecondarySlot1
            // 
            this.SecondarySlot1.HeaderText = "Secondary Slot 1";
            this.SecondarySlot1.Name = "SecondarySlot1";
            // 
            // SecondarySlot2
            // 
            this.SecondarySlot2.HeaderText = "Secondary Slot 2";
            this.SecondarySlot2.Name = "SecondarySlot2";
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            // 
            // viewRune
            // 
            this.viewRune.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.viewRune.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewRune.ForeColor = System.Drawing.Color.Blue;
            this.viewRune.Location = new System.Drawing.Point(426, 519);
            this.viewRune.Name = "viewRune";
            this.viewRune.Size = new System.Drawing.Size(163, 30);
            this.viewRune.TabIndex = 26;
            this.viewRune.Text = "View Rune Pages";
            this.viewRune.UseVisualStyleBackColor = false;
            this.viewRune.Click += new System.EventHandler(this.viewRune_Click);
            // 
            // dgViewAbilities
            // 
            this.dgViewAbilities.AllowUserToAddRows = false;
            this.dgViewAbilities.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgViewAbilities.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dgViewAbilities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgViewAbilities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.abilityName,
            this.passiveAbility,
            this.abilityShortcut,
            this.abilityRank,
            this.abilityCost,
            this.abilityDescription});
            this.dgViewAbilities.Location = new System.Drawing.Point(0, 568);
            this.dgViewAbilities.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.dgViewAbilities.Name = "dgViewAbilities";
            this.dgViewAbilities.Size = new System.Drawing.Size(1183, 120);
            this.dgViewAbilities.TabIndex = 27;
            // 
            // abilityName
            // 
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Lime;
            this.abilityName.DefaultCellStyle = dataGridViewCellStyle4;
            this.abilityName.HeaderText = "Name";
            this.abilityName.Name = "abilityName";
            // 
            // passiveAbility
            // 
            this.passiveAbility.HeaderText = "Passive ability";
            this.passiveAbility.Name = "passiveAbility";
            // 
            // abilityShortcut
            // 
            this.abilityShortcut.HeaderText = "Shortcut";
            this.abilityShortcut.Name = "abilityShortcut";
            this.abilityShortcut.Width = 104;
            // 
            // abilityRank
            // 
            this.abilityRank.HeaderText = "Rank";
            this.abilityRank.Name = "abilityRank";
            // 
            // abilityCost
            // 
            this.abilityCost.HeaderText = "Cost";
            this.abilityCost.Name = "abilityCost";
            // 
            // abilityDescription
            // 
            this.abilityDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.abilityDescription.HeaderText = "Description";
            this.abilityDescription.Name = "abilityDescription";
            // 
            // viewAbilities
            // 
            this.viewAbilities.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.viewAbilities.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewAbilities.ForeColor = System.Drawing.Color.Blue;
            this.viewAbilities.Location = new System.Drawing.Point(426, 771);
            this.viewAbilities.Name = "viewAbilities";
            this.viewAbilities.Size = new System.Drawing.Size(163, 30);
            this.viewAbilities.TabIndex = 28;
            this.viewAbilities.Text = "View Abilities";
            this.viewAbilities.UseVisualStyleBackColor = false;
            this.viewAbilities.Click += new System.EventHandler(this.viewAbilities_Click);
            // 
            // btnViewAbilities
            // 
            this.btnViewAbilities.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnViewAbilities.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewAbilities.ForeColor = System.Drawing.Color.Blue;
            this.btnViewAbilities.Location = new System.Drawing.Point(426, 695);
            this.btnViewAbilities.Name = "btnViewAbilities";
            this.btnViewAbilities.Size = new System.Drawing.Size(163, 30);
            this.btnViewAbilities.TabIndex = 29;
            this.btnViewAbilities.Text = "View Abilities";
            this.btnViewAbilities.UseVisualStyleBackColor = false;
            this.btnViewAbilities.Click += new System.EventHandler(this.btnViewAbilities_Click);
            // 
            // AllChampionsForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(1183, 749);
            this.Controls.Add(this.btnViewAbilities);
            this.Controls.Add(this.viewAbilities);
            this.Controls.Add(this.dgViewAbilities);
            this.Controls.Add(this.viewRune);
            this.Controls.Add(this.dataGridRunes);
            this.Controls.Add(this.viewItems);
            this.Controls.Add(this.dgvItems);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.dgvChampions);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AllChampionsForma";
            this.Text = "All Champions";
            this.Activated += new System.EventHandler(this.AllChampionsForma_Activated);
            this.Load += new System.EventHandler(this.AllChampionsForma_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvChampions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRunes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewAbilities)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvChampions;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn role;
        private System.Windows.Forms.DataGridViewTextBoxColumn prefLane;
        private System.Windows.Forms.DataGridViewTextBoxColumn mresource;
        private System.Windows.Forms.DataGridViewTextBoxColumn strong;
        private System.Windows.Forms.DataGridViewTextBoxColumn weak;
        private System.Windows.Forms.DataGridViewTextBoxColumn stats;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridView dgvItems;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemDescription;
        private System.Windows.Forms.Button viewItems;
        private System.Windows.Forms.DataGridView dataGridRunes;
        private System.Windows.Forms.DataGridViewTextBoxColumn RuneName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimaryRune;
        private System.Windows.Forms.DataGridViewTextBoxColumn Keystone;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimarySlot1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimarySlot2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimarySlot3;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecondaryRune;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecondarySlot1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecondarySlot2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Button viewRune;
        private System.Windows.Forms.DataGridView dgViewAbilities;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn passiveAbility;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityShortcut;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityRank;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn abilityDescription;
        private System.Windows.Forms.Button viewAbilities;
        private System.Windows.Forms.Button btnViewAbilities;
    }
}