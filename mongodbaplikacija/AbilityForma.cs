﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;

namespace mongodbaplikacija
{
    public partial class AbilityForma : Form
    {
        public AbilityForma()
        {
            InitializeComponent();
        }

        private void buttonCreateAbility_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();

            var db = client.GetDatabase("LolBaza");
            //db.DropCollection("abilityKolekcija");
            var collectionAbility = db.GetCollection<Ability>("abilityKolekcija");
            if (textBoxAbilityName.Text != "")
            {
                var a = new Ability();
                a.AbilityName = textBoxAbilityName.Text;
                a.PassiveAbility = checkBoxPassiveAbility.Checked;
                a.AbilityShortcut = textBoxAbilityShortcut.Text;
                a.AbilityRank = (int)numUDAbilityRank.Value;
                a.AbilityCost = textBoxAbilityCost.Text;
                a.AbilityDescription = textBoxAbilityDescription.Text;



                collectionAbility.InsertOne(a);
                MessageBox.Show("Ability Created: " + a.AbilityName);
            }
            else
            {
                MessageBox.Show("You have to specify ability name, first!");
            }
            //foreach (Ability ability in collectionAbility.Find(new BsonDocument()).ToList())
            //{
            //    MessageBox.Show(ability.AbilityName + ability.PassiveAbility + ability.AbilityCost 
            //        + ability.AbilityDescription + ability.AbilityRank + ability.AbilityShortcut);
            //}
        }

        private void buttonViewAbilities_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();

            var db = client.GetDatabase("LolBaza");

            var collectionAbility = db.GetCollection<Ability>("abilityKolekcija");

            dgViewAbilities.Rows.Clear();

            foreach (Ability ability in collectionAbility.Find(new BsonDocument()).ToList())
            {
                var passive = "";
                if (ability.PassiveAbility)
                    passive = "Passive";
                else
                    passive = "Not passive";

                dgViewAbilities.Rows.Insert(dgViewAbilities.Rows.Count, new object[] {ability.AbilityName, passive,
                    ability.AbilityShortcut ,  ability.AbilityRank, ability.AbilityCost, ability.AbilityDescription });
            }
        }

        private void deleteAbility_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();

            var db = client.GetDatabase("LolBaza");
            //db.DropCollection("runeKolekcija");
            var collectionAbilities = db.GetCollection<Ability>("abilityKolekcija");


            string toDel = "You didn't select the correct ability!";
            if (dgViewAbilities.SelectedRows.Count != 0)
            {
                toDel = dgViewAbilities.SelectedCells[0].Value.ToString();

                var filter = Builders<Ability>.Filter.And(
                  Builders<Ability>.Filter.Eq("AbilityName", dgViewAbilities.SelectedCells[0].Value.ToString()),
                  Builders<Ability>.Filter.Eq("AbilityDescription", dgViewAbilities.SelectedCells[3].Value.ToString())

                  );

                collectionAbilities.DeleteOne(filter);
                MessageBox.Show("Deleted one ability: " + toDel);

                dgViewAbilities.Rows.Clear();
                foreach (Ability ability in collectionAbilities.Find(new BsonDocument()).ToList())
                {
                    var passive = "";
                    if (ability.PassiveAbility)
                        passive = "Passive";
                    else
                        passive = "Not passive";

                    dgViewAbilities.Rows.Insert(dgViewAbilities.Rows.Count, new object[] {ability.AbilityName, passive,
                    ability.AbilityShortcut ,  ability.AbilityRank, ability.AbilityCost, ability.AbilityDescription });
                }

            }
            else
            {
                MessageBox.Show(toDel + " Please select the row to delete it.");
            }
        }

        private void btnSelectUpdate_Click(object sender, EventArgs e)
        {

            if (dgViewAbilities.SelectedRows.Count != 0 && dgViewAbilities.SelectedRows.Count < 2)
            {
                MessageBox.Show(dgViewAbilities.SelectedCells[0].Value.ToString() + " -> " + dgViewAbilities.SelectedCells[1].Value.ToString());
                textBoxAbilityName.Text = dgViewAbilities.SelectedCells[0].Value.ToString();
                if (dgViewAbilities.SelectedCells[1].Value.ToString() == "Passive")
                    checkBoxPassiveAbility.Checked = true;
                else
                    checkBoxPassiveAbility.Checked = false;
                textBoxAbilityCost.Text = dgViewAbilities.SelectedCells[4].Value.ToString();
                textBoxAbilityDescription.Text = dgViewAbilities.SelectedCells[5].Value.ToString();
                numUDAbilityRank.Value = (int)dgViewAbilities.SelectedCells[3].Value;
                textBoxAbilityShortcut.Text = dgViewAbilities.SelectedCells[2].Value.ToString();
            }
            else
                MessageBox.Show("You have to select one row.");

        }

        private void btnUpdateAbility_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collection = db.GetCollection<Ability>("abilityKolekcija");


            var filter = Builders<Ability>.Filter.Eq("AbilityName", textBoxAbilityName.Text);
            var result = collection.Find(filter).ToList();
            var updateDef = Builders<Ability>.Update.Set(i => i.AbilityName, textBoxAbilityName.Text).Set(i => i.PassiveAbility, checkBoxPassiveAbility.Checked).Set(i => i.AbilityCost, textBoxAbilityCost.Text).Set(
                i => i.AbilityDescription, textBoxAbilityDescription.Text).Set(i => i.AbilityRank, numUDAbilityRank.Value).Set(i => i.AbilityShortcut, textBoxAbilityShortcut.Text);

            collection.UpdateOne(filter, updateDef);

            if (result.Count != 0)
            {
                foreach (Ability ability in result)
                {
                    MessageBox.Show("Updated ability: " + ability.AbilityName);
                }
            }
            else
            {
                MessageBox.Show("You can't change ability name!");
            }

            dgViewAbilities.Rows.Clear();
            foreach (Ability ability in collection.Find(new BsonDocument()).ToList())
            {
                var passive = "";
                if (ability.PassiveAbility)
                    passive = "Passive";
                else
                    passive = "Not passive";

                dgViewAbilities.Rows.Insert(dgViewAbilities.Rows.Count, new object[] {ability.AbilityName, passive,
                    ability.AbilityShortcut ,  ability.AbilityRank, ability.AbilityCost, ability.AbilityDescription });
            }
        }
    }
}
