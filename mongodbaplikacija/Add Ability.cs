﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace mongodbaplikacija
{
    public partial class Add_Ability : Form
    {
        public Add_Ability(string champ)
        {
            InitializeComponent();
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            lblChampName.Text = champ;
            var collectionAbility = db.GetCollection<Ability>("abilityKolekcija");

            dgvAbilities.Rows.Clear();

            foreach (Ability ability in collectionAbility.Find(new BsonDocument()).ToList())
            {
                var passive = "";
                if (ability.PassiveAbility)
                    passive = "Passive";
                else
                    passive = "Not passive";

                dgvAbilities.Rows.Insert(dgvAbilities.Rows.Count, new object[] {ability.AbilityName, passive,
                    ability.AbilityShortcut ,  ability.AbilityRank, ability.AbilityCost, ability.AbilityDescription });
            }

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");

            var Ability_collection = db.GetCollection<Ability>("abilityKolekcija");

            var Champ_collection = db.GetCollection<Champion>("championKolekcija");
            var ability_Name = dgvAbilities.SelectedCells[0].Value.ToString();
            var filterability = Builders<Ability>.Filter.Eq("AbilityName", ability_Name);
            var resultability = Ability_collection.Find(filterability).ToList();

            var filterChamp = Builders<Champion>.Filter.Eq("name", lblChampName.Text);
            var resultChamp = Champ_collection.Find(filterChamp).ToList();

            if (resultability.Count != 0 && resultChamp.Count != 0)
            {
                var champ_and_abilities = resultability.Zip(resultChamp, (a, c) => new { resultability = a, resultChamp = c });
                foreach (var ac in champ_and_abilities)
                {

                    ac.resultChamp.abilities.Add(ac.resultability);
                    ac.resultability.champions.Add(ac.resultChamp.name);
                    var updateDef = Builders<Ability>.Update.Set(i => i.champions, ac.resultability.champions);
                    Ability_collection.UpdateOne(filterability, updateDef);
                    var updateDefChamp = Builders<Champion>.Update.Set(c => c.abilities, ac.resultChamp.abilities);
                    Champ_collection.UpdateOne(filterChamp, updateDefChamp);
                    MessageBox.Show("Added ability :" + ac.resultability.AbilityName + ", to " + ac.resultChamp.name + "'s abilities.");
                }
            }
            else
            {
                MessageBox.Show("Something went wrong, try again.");
            }
        }
    }
}
