﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace mongodbaplikacija
{   

    class Champion
    {
        public ObjectId Id { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public string prefferedLane { get; set; }
        public List<string> baseStats { get; set; }
        public string magicResource { get; set; }
        public string strongAgainst { get; set; }
        public string weakAgainst { get; set; }
        public List<Ability> abilities { get; set; }
        public List<Item> itemSet { get; set; }
        public List<RunePage> runes { get; set; }

        public Champion()
        {
            abilities = new List<Ability>();
            itemSet = new List<Item>();
            runes = new List<RunePage>();
            baseStats = new List<string>();
        }
    }
}
