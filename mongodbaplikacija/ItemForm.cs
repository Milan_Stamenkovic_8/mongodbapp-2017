﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace mongodbaplikacija
{
    public partial class ItemForm : Form
    {
        public ItemForm()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            //db.DropCollection("ItemKolekcija");
            var collection = db.GetCollection<Item>("ItemKolekcija");
            if (tbxItemName.Text != "")
            {
                Item item = new Item();
                if (tbxItemName.Text != "")
                {
                    item.itemName = tbxItemName.Text;
                }
                if (cbxStartingItems.Checked)
                {
                    item.itemType.Add("Starting Items");
                }
                if (cbxTools.Checked)
                {
                    item.itemType.Add("Tools");
                }
                if (cbxDefense.Checked)
                {
                    item.itemType.Add("Defense");
                }
                if (cbxAttack.Checked)
                {
                    item.itemType.Add("Attack");
                }
                if (cbxMagic.Checked)
                {
                    item.itemType.Add("Magic");
                }
                if (cbxMovement.Checked)
                {
                    item.itemType.Add("Movement");
                }
                if (tbxitemDescription.Text != "")
                {
                    item.itemDescription = tbxitemDescription.Text;
                }
                item.itemCost = (int)nudItemCost.Value;

                collection.InsertOne(item);
                MessageBox.Show("Created item: " + item.itemName);
            }
            else
            {
                MessageBox.Show("You have to sepcify the item name!"); 
            }

        }

        private void btnView_Click(object sender, EventArgs e)
        {
            var ViewItems = new ViewItems();
            ViewItems.Show();
           
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
           

           
        }
    }
}
