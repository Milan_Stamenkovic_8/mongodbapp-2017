﻿namespace mongodbaplikacija
{
    partial class Lolnexus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Lolnexus));
            this.addchamp = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lolbuild = new System.Windows.Forms.Button();
            this.lolrunes = new System.Windows.Forms.Button();
            this.lolspells = new System.Windows.Forms.Button();
            this.quit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // addchamp
            // 
            this.addchamp.BackColor = System.Drawing.Color.Black;
            this.addchamp.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addchamp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.addchamp.Location = new System.Drawing.Point(74, 271);
            this.addchamp.Name = "addchamp";
            this.addchamp.Size = new System.Drawing.Size(123, 23);
            this.addchamp.TabIndex = 0;
            this.addchamp.Text = "Add Champion";
            this.addchamp.UseVisualStyleBackColor = false;
            this.addchamp.Click += new System.EventHandler(this.addchamp_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button2.Location = new System.Drawing.Point(226, 271);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(135, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "All Champions";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lolbuild
            // 
            this.lolbuild.BackColor = System.Drawing.Color.Black;
            this.lolbuild.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lolbuild.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lolbuild.Location = new System.Drawing.Point(12, 27);
            this.lolbuild.Name = "lolbuild";
            this.lolbuild.Size = new System.Drawing.Size(120, 35);
            this.lolbuild.TabIndex = 3;
            this.lolbuild.Text = "Item Builds";
            this.lolbuild.UseVisualStyleBackColor = false;
            this.lolbuild.Click += new System.EventHandler(this.lolbuild_Click);
            // 
            // lolrunes
            // 
            this.lolrunes.BackColor = System.Drawing.Color.Black;
            this.lolrunes.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lolrunes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lolrunes.Location = new System.Drawing.Point(153, 27);
            this.lolrunes.Name = "lolrunes";
            this.lolrunes.Size = new System.Drawing.Size(100, 35);
            this.lolrunes.TabIndex = 4;
            this.lolrunes.Text = "Rune Tree";
            this.lolrunes.UseVisualStyleBackColor = false;
            this.lolrunes.Click += new System.EventHandler(this.lolrunes_Click);
            // 
            // lolspells
            // 
            this.lolspells.BackColor = System.Drawing.Color.Black;
            this.lolspells.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lolspells.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lolspells.Location = new System.Drawing.Point(270, 27);
            this.lolspells.Name = "lolspells";
            this.lolspells.Size = new System.Drawing.Size(149, 35);
            this.lolspells.TabIndex = 5;
            this.lolspells.Text = "Ability Leveling";
            this.lolspells.UseVisualStyleBackColor = false;
            this.lolspells.Click += new System.EventHandler(this.lolspells_Click);
            // 
            // quit
            // 
            this.quit.BackColor = System.Drawing.Color.Black;
            this.quit.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.quit.Location = new System.Drawing.Point(178, 320);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(75, 23);
            this.quit.TabIndex = 6;
            this.quit.Text = "Exit";
            this.quit.UseVisualStyleBackColor = false;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::mongodbaplikacija.Properties.Resources.download;
            this.pictureBox1.Location = new System.Drawing.Point(34, 98);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(361, 139);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // Lolnexus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(433, 355);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.lolspells);
            this.Controls.Add(this.lolrunes);
            this.Controls.Add(this.lolbuild);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.addchamp);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Lolnexus";
            this.Text = "LoLNexus";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addchamp;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button lolbuild;
        private System.Windows.Forms.Button lolrunes;
        private System.Windows.Forms.Button lolspells;
        private System.Windows.Forms.Button quit;
    }
}

