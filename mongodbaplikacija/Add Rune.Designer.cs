﻿namespace mongodbaplikacija
{
    partial class Add_Rune
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Rune));
            this.label2 = new System.Windows.Forms.Label();
            this.lblChampName = new System.Windows.Forms.Label();
            this.dataGridRunes = new System.Windows.Forms.DataGridView();
            this.RuneName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimaryRune = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Keystone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimarySlot1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimarySlot2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrimarySlot3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecondaryRune = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecondarySlot1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecondarySlot2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRunes)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Yellow;
            this.label2.Location = new System.Drawing.Point(12, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Current Champion:";
            // 
            // lblChampName
            // 
            this.lblChampName.AutoSize = true;
            this.lblChampName.Location = new System.Drawing.Point(175, 26);
            this.lblChampName.Name = "lblChampName";
            this.lblChampName.Size = new System.Drawing.Size(0, 17);
            this.lblChampName.TabIndex = 5;
            // 
            // dataGridRunes
            // 
            this.dataGridRunes.AllowUserToAddRows = false;
            this.dataGridRunes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dataGridRunes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRunes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RuneName,
            this.PrimaryRune,
            this.Keystone,
            this.PrimarySlot1,
            this.PrimarySlot2,
            this.PrimarySlot3,
            this.SecondaryRune,
            this.SecondarySlot1,
            this.SecondarySlot2});
            this.dataGridRunes.Location = new System.Drawing.Point(10, 103);
            this.dataGridRunes.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.dataGridRunes.Name = "dataGridRunes";
            this.dataGridRunes.Size = new System.Drawing.Size(997, 196);
            this.dataGridRunes.TabIndex = 25;
            // 
            // RuneName
            // 
            this.RuneName.HeaderText = "Rune Name";
            this.RuneName.Name = "RuneName";
            // 
            // PrimaryRune
            // 
            this.PrimaryRune.HeaderText = "Primary rune";
            this.PrimaryRune.Name = "PrimaryRune";
            // 
            // Keystone
            // 
            this.Keystone.HeaderText = "Keystone";
            this.Keystone.Name = "Keystone";
            // 
            // PrimarySlot1
            // 
            this.PrimarySlot1.HeaderText = "Primary Slot1";
            this.PrimarySlot1.Name = "PrimarySlot1";
            // 
            // PrimarySlot2
            // 
            this.PrimarySlot2.HeaderText = "Primary Slot 2";
            this.PrimarySlot2.Name = "PrimarySlot2";
            // 
            // PrimarySlot3
            // 
            this.PrimarySlot3.HeaderText = "Primary Slot 3";
            this.PrimarySlot3.Name = "PrimarySlot3";
            // 
            // SecondaryRune
            // 
            this.SecondaryRune.HeaderText = "Secondary rune";
            this.SecondaryRune.Name = "SecondaryRune";
            // 
            // SecondarySlot1
            // 
            this.SecondarySlot1.HeaderText = "Secondary Slot 1";
            this.SecondarySlot1.Name = "SecondarySlot1";
            // 
            // SecondarySlot2
            // 
            this.SecondarySlot2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SecondarySlot2.HeaderText = "Secondary Slot 2";
            this.SecondarySlot2.Name = "SecondarySlot2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(15, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(319, 17);
            this.label1.TabIndex = 26;
            this.label1.Text = "Select rune page from the following list:";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnAdd.ForeColor = System.Drawing.Color.Yellow;
            this.btnAdd.Location = new System.Drawing.Point(422, 315);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(116, 33);
            this.btnAdd.TabIndex = 27;
            this.btnAdd.Text = "Add Rune Page";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // Add_Rune
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(1021, 360);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridRunes);
            this.Controls.Add(this.lblChampName);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Snap ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "Add_Rune";
            this.Text = "Add Rune";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRunes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblChampName;
        private System.Windows.Forms.DataGridView dataGridRunes;
        private System.Windows.Forms.DataGridViewTextBoxColumn RuneName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimaryRune;
        private System.Windows.Forms.DataGridViewTextBoxColumn Keystone;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimarySlot1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimarySlot2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrimarySlot3;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecondaryRune;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecondarySlot1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecondarySlot2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAdd;
    }
}