﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace mongodbaplikacija
{
    public partial class ViewItems : Form
    {
        public ViewItems()
        {
            InitializeComponent();
        }
        public void ResetDGV()
        {
            dgvItems.Update();
            dgvItems.Refresh();
        }

       
      

        private void runePageBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void dgvItems_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void runePageBindingSource_CurrentChanged_1(object sender, EventArgs e)
        {

        }

        private void ViewItems_Load(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collection = db.GetCollection<Item>("ItemKolekcija");
            foreach (Item items in collection.Find(new BsonDocument()).ToList())
            {
                var index = dgvItems.Rows.Add();
                dgvItems.Rows[index].Cells["itemName"].Value = items.itemName;
                if (items.itemType.Count() > 1)
                {
                    string itemtype = string.Join(",", items.itemType.ToArray());
                    dgvItems.Rows[index].Cells["itemType"].Value = itemtype;
                }
                dgvItems.Rows[index].Cells["itemCost"].Value = items.itemCost;
                dgvItems.Rows[index].Cells["itemDescription"].Value = items.itemDescription;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            var UpdateItem = new UpdateItem();
            
            UpdateItem.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collection = db.GetCollection<Item>("ItemKolekcija");
            string toDel = "You didn't select the correct item!";
            if (dgvItems.SelectedCells[0].Value != null)
            {
                toDel = dgvItems.SelectedCells[0].Value.ToString();

                var filter = Builders<Item>.Filter.Eq("itemName", toDel);

                collection.DeleteOne(filter);
                MessageBox.Show("Deleted one item: " + toDel);
            }
            else
            {
                MessageBox.Show(toDel + " Please select the item name to delete it.");
            }
           
        }

        private void ViewItems_Activated(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            var collection = db.GetCollection<Item>("ItemKolekcija");
            dgvItems.Rows.Clear();
            foreach (Item items in collection.Find(new BsonDocument()).ToList())
            {
                var index = dgvItems.Rows.Add();
                dgvItems.Rows[index].Cells["itemName"].Value = items.itemName;
                if (items.itemType.Count() > 1)
                {
                    string itemtype = string.Join(",", items.itemType.ToArray());
                    dgvItems.Rows[index].Cells["itemType"].Value = itemtype;
                }
                dgvItems.Rows[index].Cells["itemCost"].Value = items.itemCost;
                dgvItems.Rows[index].Cells["itemDescription"].Value = items.itemDescription;
            }
        }
    }
}
