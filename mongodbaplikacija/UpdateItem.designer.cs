﻿namespace mongodbaplikacija
{
    partial class UpdateItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateItem));
            this.lbxItemName = new System.Windows.Forms.Label();
            this.tbxItemName = new System.Windows.Forms.TextBox();
            this.lblItemDescript = new System.Windows.Forms.Label();
            this.tbxitemDescription = new System.Windows.Forms.TextBox();
            this.lbxItemCost = new System.Windows.Forms.Label();
            this.nudItemCost = new System.Windows.Forms.NumericUpDown();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudItemCost)).BeginInit();
            this.SuspendLayout();
            // 
            // lbxItemName
            // 
            this.lbxItemName.AutoSize = true;
            this.lbxItemName.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxItemName.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lbxItemName.Location = new System.Drawing.Point(12, 9);
            this.lbxItemName.Name = "lbxItemName";
            this.lbxItemName.Size = new System.Drawing.Size(109, 19);
            this.lbxItemName.TabIndex = 2;
            this.lbxItemName.Text = "Item Name:";
            // 
            // tbxItemName
            // 
            this.tbxItemName.Location = new System.Drawing.Point(12, 31);
            this.tbxItemName.Name = "tbxItemName";
            this.tbxItemName.Size = new System.Drawing.Size(233, 20);
            this.tbxItemName.TabIndex = 3;
            // 
            // lblItemDescript
            // 
            this.lblItemDescript.AutoSize = true;
            this.lblItemDescript.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemDescript.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblItemDescript.Location = new System.Drawing.Point(12, 66);
            this.lblItemDescript.Name = "lblItemDescript";
            this.lblItemDescript.Size = new System.Drawing.Size(162, 19);
            this.lblItemDescript.TabIndex = 7;
            this.lblItemDescript.Text = "Item Description:";
            // 
            // tbxitemDescription
            // 
            this.tbxitemDescription.Location = new System.Drawing.Point(12, 88);
            this.tbxitemDescription.Multiline = true;
            this.tbxitemDescription.Name = "tbxitemDescription";
            this.tbxitemDescription.Size = new System.Drawing.Size(233, 84);
            this.tbxitemDescription.TabIndex = 8;
            // 
            // lbxItemCost
            // 
            this.lbxItemCost.AutoSize = true;
            this.lbxItemCost.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxItemCost.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lbxItemCost.Location = new System.Drawing.Point(12, 185);
            this.lbxItemCost.Name = "lbxItemCost";
            this.lbxItemCost.Size = new System.Drawing.Size(101, 19);
            this.lbxItemCost.TabIndex = 9;
            this.lbxItemCost.Text = "Item Cost:";
            // 
            // nudItemCost
            // 
            this.nudItemCost.Increment = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.nudItemCost.Location = new System.Drawing.Point(125, 184);
            this.nudItemCost.Maximum = new decimal(new int[] {
            3700,
            0,
            0,
            0});
            this.nudItemCost.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudItemCost.Name = "nudItemCost";
            this.nudItemCost.Size = new System.Drawing.Size(120, 20);
            this.nudItemCost.TabIndex = 10;
            this.nudItemCost.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUpdate.Font = new System.Drawing.Font("Ravie", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.Lime;
            this.btnUpdate.Location = new System.Drawing.Point(48, 214);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(164, 35);
            this.btnUpdate.TabIndex = 12;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // UpdateItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.nudItemCost);
            this.Controls.Add(this.lbxItemCost);
            this.Controls.Add(this.tbxitemDescription);
            this.Controls.Add(this.lblItemDescript);
            this.Controls.Add(this.tbxItemName);
            this.Controls.Add(this.lbxItemName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UpdateItem";
            this.Text = "UpdateItem";
            ((System.ComponentModel.ISupportInitialize)(this.nudItemCost)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbxItemName;
        private System.Windows.Forms.TextBox tbxItemName;
        private System.Windows.Forms.Label lblItemDescript;
        private System.Windows.Forms.TextBox tbxitemDescription;
        private System.Windows.Forms.Label lbxItemCost;
        private System.Windows.Forms.NumericUpDown nudItemCost;
        private System.Windows.Forms.Button btnUpdate;
    }
}