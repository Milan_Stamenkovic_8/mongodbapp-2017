﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace mongodbaplikacija
{
    public partial class Add_Item : Form
    {
        public Add_Item(string champ)
        {
            InitializeComponent();
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");
            lblChampName.Text = champ;
            var Item_collection = db.GetCollection<Item>("ItemKolekcija");
            dataGridAddItem.Rows.Clear();
            foreach (Item item in Item_collection.Find(new BsonDocument()).ToList())
            {
                dataGridAddItem.Rows.Insert(dataGridAddItem.Rows.Count, new object[] {item.itemName, item.itemType, item.itemCost, item.itemDescription});
            }
            
        }

        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var client = new MongoClient();
            var db = client.GetDatabase("LolBaza");

            var Item_collection = db.GetCollection<Item>("ItemKolekcija");

            var Champ_collection = db.GetCollection<Champion>("championKolekcija");
            var item_Name = dataGridAddItem.SelectedCells[0].Value.ToString();
            var filterItem = Builders<Item>.Filter.Eq("itemName", item_Name);
            var resultItem = Item_collection.Find(filterItem).ToList();

            var filterChamp = Builders<Champion>.Filter.Eq("name", lblChampName.Text);
            var resultChamp = Champ_collection.Find(filterChamp).ToList();

            if (resultItem.Count != 0 && resultChamp.Count != 0)
            {
                var champ_and_items = resultItem.Zip(resultChamp, (i, c) => new { resultItem = i, resultChamp = c });
                foreach(var ic in champ_and_items)
                {

                    ic.resultChamp.itemSet.Add(ic.resultItem);
                    ic.resultItem.champions.Add(ic.resultChamp.name);
                    var updateDef = Builders<Item>.Update.Set(i => i.champions, ic.resultItem.champions);
                    Item_collection.UpdateOne(filterItem, updateDef);
                    var updateDefChamp = Builders<Champion>.Update.Set(c => c.itemSet, ic.resultChamp.itemSet);
                    Champ_collection.UpdateOne(filterChamp, updateDefChamp);
                    MessageBox.Show("Added item :"+ic.resultItem.itemName +  ", to " + ic.resultChamp.name + "'s item set." );
                }
            }
            else
            {
                MessageBox.Show("Something went wrong, try again.");
            }
        }
    }
}
